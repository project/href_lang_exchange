<?php

/**
 * @file
 * Allow to extend the href lang support.
 */

/**
 * Implements hook_href_lang_autocomplete_alter().
 */
function hook_href_lang_autocomplete_alter($query, $request) {
}

/**
 * Implements hook_href_lang_xml_sitemap_alter().
 */
function hook_href_lang_xml_sitemap_alter($entity, $link, $href_lang) {
}

/**
 * Implements hook_href_lang_autocomplete().
 */
function hook_href_lang_autocomplete() {
}

/**
 * Implements hook_jsonapi_autocomplete_filter_results_alter().
 */
function hook_jsonapi_autocomplete_filter_results_alter($matches) {
};

/**
 * Implements hook_href_lang_head_links_alter().
 */
function hook_href_lang_head_links_alter() {
}

/**
 * Implements hook_alter_href_lang_list_alter().
 */
function hook_alter_href_lang_list_alter($languages) {
};

/**
 * Implements hook_possible_language_options_alter().
 */
function hook_possible_language_options_alter($select_options) {
};
