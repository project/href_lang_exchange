<?php

namespace Drupal\href_lang_exchange\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\href_lang_exchange\HrefLangItemInterface;
use Drupal\href_lang_exchange\HrefLangItemState;
use Drupal\user\UserInterface;

/**
 * Defines the HrefLangItem entity.
 *
 * @ContentEntityType(
 *   id = "href_lang_item",
 *   label = @Translation("HrefLangItem"),
 *   handlers = {
 *     "storage" = "Drupal\href_lang_exchange\HrefLangItemStorage",
 *     "storage_schema" = "Drupal\href_lang_exchange\HrefLangItemSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\href_lang_exchange\Form\HrefLangItemForm",
 *       "edit" = "Drupal\href_lang_exchange\Form\HrefLangItemForm",
 *       "delete" = "Drupal\href_lang_exchange\Form\HrefLangItemDeleteForm",
 *     },
 *    "access" = "Drupal\href_lang_exchange\HrefLangItemAccessControlHandler",
 *   },
 *   base_table = "href_lang_item",
 *   admin_permission = "administer href_lang_item entity",
 *   fieldable = FALSE,
 *   links = {
 *     "canonical" = "/href_lang_item/{href_lang_item}",
 *     "add-form" = "/href_lang_item/add",
 *     "edit-form" = "/href_lang_item/{href_lang_item}/edit",
 *     "delete-form" = "/href_lang_item/{href_lang_item}/delete",
 *   },
 *   entity_keys = {
 *     "uuid" = "uuid",
 *     "id" = "id",
 *   }
 * )
 */
class HrefLangItem extends ContentEntityBase implements HrefLangItemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    return $this->get('uuid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguage() {
    return $this->get('language')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getGid() {
    return $this->get('gid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->get('path')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->get('path')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegion() {
    return $this->get('region')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalId() {
    return $this->get('internal_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function toJsonApi() {
    $default = [
      'data' => [
        'type' => 'href_lang_item--href_lang_item',
        'attributes' => [
          'gid' => $this->getGid(),
          'region' => $this->getRegion(),
          'language' => $this->getLanguage(),
          'internal_id' => $this->getInternalId(),
          'title' => $this->getTitle(),
          'path' => $this->getPath(),
        ],
      ],
    ];

    if (!empty($this->uuid())) {
      $default['data']['id'] = $this->uuid();
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegion($region) {
    $this->set('region', $region);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($path) {
    $this->set('path', $path);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setGid($gid) {
    $this->set('gid', $gid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setLanguage($language) {
    $this->set('language', $language);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the test entity.'))
      ->setReadOnly(TRUE);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['gid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Group Identifier'))
      ->setDescription(t('A gid for the href lang item.'))
      ->setSettings(
        [
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ]
      )
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -5,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -5,
        ]
      )->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['region'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Region'))
      ->setRequired(TRUE)
      ->setStorageRequired(TRUE)
      ->setDescription(t('The region for the href.'))
      ->setSettings(
        [
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ]
      )
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -6,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -6,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['language'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Language'))
      ->setRequired(TRUE)
      ->setStorageRequired(TRUE)
      ->setDescription(t('The langcode of the language.'))
      ->setSettings(
        [
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ]
      )
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -6,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -6,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['internal_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('A internal id'))
      ->setDescription(t('A internal id for the content.'))
      ->setSettings(
        [
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ]
      )
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -6,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -6,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setStorageRequired(TRUE)
      ->setDescription(t('The title of the page.'))
      ->setSettings(
        [
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ]
      )
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -6,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -6,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['path'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Path'))
      ->setDescription(t('The path for the href lang.'))
      ->setSettings(
        [
          'default_value' => '',
          'max_length' => 255,
          'text_processing' => 0,
        ]
      )
      ->setDisplayOptions(
        'view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => -5,
        ]
      )
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
          'weight' => -5,
        ]
      )->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Status'))
      ->setDescription(t('The site to which the item belongs.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 20,
      ])
      ->setSetting('size', 'big')->setDefaultValue(HrefLangItemState::PUBLISHED)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    Cache::invalidateTags(['language_switch']);
  }

}
