<?php

namespace Drupal\href_lang_exchange\Authorization;

use Drupal\href_lang_exchange\Connection\AuthorizationInterface;

/**
 * Class No Authorization.
 *
 * @package Drupal\href_lang_exchange\Authorization
 */
class NoAuthorization implements AuthorizationInterface {

  /**
   * {@inheritdoc}
   */
  public function getAuthHeader() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function sendAuthHeader() {
    return FALSE;
  }

}
