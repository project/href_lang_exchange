<?php

namespace Drupal\href_lang_exchange\Connection;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\href_lang_exchange\HrefLangItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Send Validator.
 *
 * @package Drupal\href_lang_exchange\Connection
 */
class SendValidator implements SendValidatorInterface {

  /**
   * The connection object.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $connection;

  /**
   * The request manager.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The route matcher.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatcher;

  /**
   * The resource manager.
   *
   * @var \Drupal\href_lang_exchange\Connection\ResourceInterface
   */
  protected $resourceManager;

  /**
   * SendValidator constructor.
   *
   * @param \Drupal\href_lang_exchange\Connection\ConnectionInterface $connection
   *   The connection object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_matcher
   *   The route matcher.
   * @param \Drupal\href_lang_exchange\Connection\ResourceInterface $resource_manager
   *   The resource manager.
   */
  public function __construct(ConnectionInterface $connection, RequestStack $request, RouteMatchInterface $route_matcher, ResourceInterface $resource_manager) {
    $this->connection = $connection;
    $this->request = $request;
    $this->routeMatcher = $route_matcher;
    $this->resourceManager = $resource_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function isItJsonApiRequest(EntityInterface $entity) {
    return $this->connection->checkSelfMaster() &&
      $entity instanceof HrefLangItemInterface &&
      ($this->request->getCurrentRequest()->isMethod('POST') ||
      $this->request->getCurrentRequest()->isMethod('PATCH')) &&
      !($this->request->getCurrentRequest()->attributes->has('_create_multiple'))&&
      ($this->startsWith($this->request->getCurrentRequest()
        ->getRequestUri(), $this->resourceManager->getResourceAddress()));
  }

  /**
   * {@inheritDoc}
   */
  public function isItFormEdit(EntityInterface $entity) {
    return $this->isRoute('entity.href_lang_item.edit_form', $this->routeMatcher->getRouteName(), $entity);
  }

  /**
   * {@inheritDoc}
   */
  public function isItFormAdd(EntityInterface $entity) {
    return $this->isRoute('href_lang_item.href_lang_item_add', $this->routeMatcher->getRouteName(), $entity);
  }

  /**
   * {@inheritDoc}
   */
  private function isRoute($key, $route_name, EntityInterface $entity) {
    return $this->connection->checkSelfMaster() &&
      $entity instanceof HrefLangItemInterface &&
      $this->request->getCurrentRequest()->isMethod('POST') &&
      is_string($route_name) &&
      (strcmp($route_name, $key) == 0);
  }

  /**
   * Check if a string starts with another string.
   *
   * @param string $string
   *   The basic string.
   * @param string $startString
   *   The needle.
   *
   * @return bool
   *   return true if the string start with $startstring.
   */
  private function startsWith($string, $startString) {

    if (
      (!(is_string($string) && is_string($startString))) ||
      (empty($startString) || empty($string))
    ) {
      return FALSE;
    }

    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
  }

}
