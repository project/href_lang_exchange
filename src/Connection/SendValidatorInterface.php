<?php

namespace Drupal\href_lang_exchange\Connection;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface Send Validator Interface.
 *
 * @package Drupal\href_lang_exchange\Connection
 */
interface SendValidatorInterface {

  /**
   * This method check if the current request a json api request.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the current validation should operate upon.
   *
   * @return bool
   *   Return true if it come from a json api request otherwise false.
   */
  public function isItJsonApiRequest(EntityInterface $entity);

  /**
   * This method check if the request come from the form edit of href lang.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the current validation should operate upon.
   *
   * @return bool
   *   Return true if it come from the edit form otherwise false.
   */
  public function isItFormEdit(EntityInterface $entity);

  /**
   * This method check if the request come from the form add of href lang.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the current validation should operate upon.
   *
   * @return bool
   *   Return true if it come from the edit form otherwise false.
   */
  public function isItFormAdd(EntityInterface $entity);

}
