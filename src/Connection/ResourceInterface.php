<?php

namespace Drupal\href_lang_exchange\Connection;

/**
 * Interface Resource Interface.
 *
 * @package Drupal\href_lang_exchange\Connection
 */
interface ResourceInterface {

  /**
   * Return the uri to the resource.
   *
   * @return string
   *   The uri of the resource.
   */
  public function getResourceAddress();

}
