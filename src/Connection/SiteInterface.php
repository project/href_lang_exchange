<?php

namespace Drupal\href_lang_exchange\Connection;

/**
 * Interface Site Interface.
 *
 * @package Drupal\href_lang_exchange\Connection
 */
interface SiteInterface {

  /**
   * The return url.
   *
   * @return string
   *   The url.
   */
  public function getUrl();

}
