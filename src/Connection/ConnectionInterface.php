<?php

namespace Drupal\href_lang_exchange\Connection;

use Drupal\Core\Entity\EntityInterface;

/**
 * Class Connection Interface.
 */
interface ConnectionInterface {

  /**
   * Get the master site.
   *
   * @return SiteInterface
   *   Return a object that implements MasterSiteInterface
   *
   * @throws \Exception
   */
  public function getMaster();

  /**
   * Delete a href lang item from the json api.
   *
   * @param string $url
   *   The url of the destination site.
   * @param array $values
   *   The values for the href lang item.
   * @param bool $queue_only
   *   The queue only option.
   */
  public function deleteHrefLangItem($url, array $values, $queue_only = FALSE);

  /**
   * Update a href lang item from the json api.
   *
   * @param string $url
   *   The url of the destination site.
   * @param array $values
   *   The values for the href lang item.
   *
   * @return array
   *   The hreflang object as array.
   */
  public function updateHrefLangItem($url, array $values);

  /**
   * Get a href lang item from the json api.
   *
   * @param string $url
   *   The url of the destination site.
   * @param string $filter
   *   Currently a small workaround to allow filter queries.
   * @param array $ignore_response
   *   Add status codes that should be ignored.
   *
   * @return array
   *   The hreflang object as array.
   *
   * @throws \Exception
   */
  public function getHrefLangItem($url, $filter, array $ignore_response = []);

  /**
   * This method create a href lang item from a entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The base entity.
   * @param string $gid
   *   The group id.
   *
   * @return \Drupal\href_lang_exchange\Entity\HrefLangItem
   *   A href lang item from the base entity values.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createHrefLangItemFromEntity(EntityInterface $entity, $gid);

  /**
   * Check if you are the master instance.
   *
   * @return bool
   *   Return true if you are the master otherwise false.
   */
  public function checkSelfMaster();

  /**
   * Create a href lang item on a destination site.
   *
   * @param string $url
   *   The url of the destination site.
   * @param array $values
   *   The values for the href lang item.
   * @param bool $queue_only
   *   Only queue items.
   */
  public function createHrefLangItem($url, array $values, $queue_only = FALSE);

  /**
   * Create a href lang item on a destination site (async).
   *
   * @param string $url
   *   The url of the destination site.
   * @param array $values
   *   The values for the href lang item.
   */
  public function createHrefLangItemAsync($url, array $values);

  /**
   * Delete a href lang item from the json api.
   *
   * @param string $url
   *   The url of the destination site.
   * @param array $values
   *   The values for the href lang item.
   */
  public function deleteHrefLangItemAsync($url, array $values);

  /**
   * Search for a field in a Entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity you should look in.
   * @param string $field_name
   *   The name of the field that should be found.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   Return a field definition or null.
   */
  public function getEntityField(EntityInterface $entity, $field_name);

  /**
   * Return a array of autocomplete items.
   *
   * @param string $filter
   *   Currently a small workaround to allow filter queries.
   * @param array $ignore_response
   *   Add status codes that should be ignored.
   *
   * @return array
   *   The a list of hreflang like object as array.
   */
  public function getAutocompleteItem($filter, array $ignore_response = []);

  /**
   * Pauses the synchronization for a given time.
   *
   * @param string $time
   *   The time to pause the sync.
   */
  public function pauseSyncTemporary(string $time = '10 minutes');

  /**
   * Resume the synchronization.
   */
  public function resumeSync();

  /**
   * Check if the synchronization is pasued.
   *
   * @return bool
   *   True if the synchronization is paused.
   */
  public function isSyncPaused();

}
