<?php

namespace Drupal\href_lang_exchange\Connection;

/**
 * Interface Authorization Interface.
 */
interface AuthorizationInterface {

  /**
   * The return a authHeader.
   *
   * @return string
   *   The url.
   */
  public function getAuthHeader();

  /**
   * Set if you want authHeader.
   *
   * @return bool
   *   Return true if authHeader exist and false if not.
   */
  public function sendAuthHeader();

}
