<?php

namespace Drupal\href_lang_exchange\Connection;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\href_lang_exchange\Plugin\QueueWorker\HrefLangUpdateQueue;
use Drupal\href_lang_exchange\Service\NormalLazyStore;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\href_lang_exchange\Entity\HrefLangItem;
use Drupal\href_lang_exchange\HrefLangItemInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * File Connection class.
 *
 * @package Drupal\href_lang_exchange
 */
class Connection implements ConnectionInterface, ResourceInterface {


  const RESOURCE_ADDRESS = '/jsonapi/href_lang_item/href_lang_item';

  const OPTIONS = [
    'timeout' => 200,
    'headers' => [
      'Content-type' => 'application/vnd.api+json',
    ],
  ];

  const QUEUE_RESEND = 'resend_creation_href_lang_queue';

  /**
   * A Guzzle Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * A Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The Psr Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A authorization handler.
   *
   * @var \Drupal\href_lang_exchange\Connection\AuthorizationInterface
   */
  protected $auth;

  /**
   * The site manager.
   *
   * @var \Drupal\href_lang_exchange\Connection\SiteManagementInterface
   */
  protected $siteManagement;

  /**
   * The keyValue storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * Connection constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The client interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The client interface.
   * @param \Psr\Log\LoggerInterface $logger
   *   The psr logger.
   * @param \Drupal\href_lang_exchange\Connection\AuthorizationInterface $auth
   *   A authorization handler.
   * @param \Drupal\href_lang_exchange\Connection\SiteManagementInterface $site_management
   *   A site manager.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   The keyValue service.
   */
  public function __construct(ClientInterface $client, QueueFactory $queue, LoggerInterface $logger, AuthorizationInterface $auth, SiteManagementInterface $site_management, KeyValueFactoryInterface $key_value) {
    $this->httpClient = $client;
    $this->queue = $queue;
    $this->logger = $logger;
    $this->auth = $auth;
    $this->siteManagement = $site_management;
    $this->keyValue = $key_value->get('href_lang_exchange');
  }

  /**
   * {@inheritDoc}
   */
  public function getHrefLangItem($url, $filter, $ignore_response = []) {

    if (!is_string($url) || !is_string($filter)) {
      throw new \Exception('$url or $filter is not a string.');
    }

    $options = CONNECTION::OPTIONS;

    if ($this->auth->sendAuthHeader()) {
      $options['headers']['Authorization'] = $this->auth->getAuthHeader();
    }

    $element['url'] = $url . $this->getResourceAddress() . $filter;
    $element['options'] = $options;

    $response = NULL;
    try {
      $response = $this->httpClient->get(
        $element['url'],
        $element['options']
      );
    }
    catch (BadResponseException $e) {
      $this->handleExceptionResponse($e, $ignore_response, $url);
    }
    catch (\Exception $e) {
      $this->logger->alert(sprintf('GET: %s :: Message: %s', $element['url'], $e->getMessage()));
      $response = NULL;
    }

    if (!empty($response)) {
      $response = (string) $response->getBody();
      $result = json_decode($response, TRUE);
      if (isset($result['data']) && is_array($result['data']) && !empty($result['data'])) {
        $response = [];
        if (!isset($result['data']) && !empty($result['data'])) {
          $response['data'] = array_shift($result['data']);
        }
        else {
          $response['data'] = $result['data'];
        }
        return $response;
      }
      return [];
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function updateHrefLangItem($url, array $values) {
    $response = NULL;

    $options = CONNECTION::OPTIONS;
    $options['body'] = json_encode($values);

    if ($this->auth->sendAuthHeader()) {
      $options['headers']['Authorization'] = $this->auth->getAuthHeader();
    }

    $element['url'] = $url;
    $element['values'] = $values;

    if ($this->auth->sendAuthHeader()) {
      $options['headers']['Authorization'] = $this->auth->getAuthHeader();
    }

    if (!isset($values['data']['id'])) {
      $this->logger->alert('Update ist not possible without uuid.');
    }

    $path_extension = "/";
    $path_extension .= $values['data']['id'];

    $element['url'] = $url . $this->getResourceAddress() . $path_extension;
    $element['options'] = $options;

    try {
      $response = $this->httpClient->patch(
        $element['url'],
        $element['options']
      );
    }
    catch (BadResponseException  $e) {
      $this->handleExceptionResponse($e, [], $url);
      $response = [];
    }
    catch (\Exception $e) {
      $this->logger->alert(sprintf('PATCH: %s :: Message: %s', $element['url'], $e->getMessage()));
      $response = [];
    }

    return $response;

  }

  /**
   * {@inheritDoc}
   */
  public function deleteHrefLangItem($url, array $values, $queue_only = FALSE) {
    $options = CONNECTION::OPTIONS;

    if ($this->auth->sendAuthHeader()) {
      $options['headers']['Authorization'] = $this->auth->getAuthHeader();
    }

    if (!isset($values['data']['id'])) {
      $this->logger->alert('Delete ist not possible without uuid.');
      return;
    }

    $path_extension = '/' . $values['data']['id'];

    $element['url'] = $url;
    $element['options'] = $options;
    $element['values'] = $values;
    $element['type'] = HrefLangUpdateQueue::DELETE_LANG_ITEM;

    if ($queue_only == TRUE) {
      $queue = $this->queue->get(Connection::QUEUE_RESEND);
      $queue->createItem($element);
      return;
    }

    try {
      $response = $this->httpClient->delete(
        $element['url'] . $this->getResourceAddress() . $path_extension,
        $element['options']
      );
    }
    catch (BadResponseException  $e) {

      $this->handleExceptionResponse($e, [404], $url);

    }
    catch (\Exception $e) {
      $this->logger->alert(sprintf('DELETE: %s :: Message: %s', $element['url'], $e->getMessage()));
    }

  }

  /**
   * {@inheritDoc}
   */
  public function deleteHrefLangItemAsync($url, array $values) {
    $lazy_store = \Drupal::service('href_lang_exchange.service.lazy_store');
    /** @var \Drupal\href_lang_exchange\Service\SettingsManagerInterface $settings_manager */
    $settings_manager = \Drupal::service('href_lang_exchange.helper.settings_manager');
    $lazy_store->addItem(new NormalLazyStore($this, 'deleteHrefLangItem', [
      $url,
      $values,
      $settings_manager->isClientQueued(),
    ]));
  }

  /**
   * {@inheritDoc}
   */
  public function createHrefLangItem($url, array $values, $queue_only = FALSE) {
    $response = NULL;

    $options = CONNECTION::OPTIONS;
    $options['body'] = json_encode($values);

    if ($this->auth->sendAuthHeader()) {
      $options['headers']['Authorization'] = $this->auth->getAuthHeader();
    }

    $element['url'] = $url;
    $element['values'] = $values;
    $element['type'] = HrefLangUpdateQueue::CREATE_LANG_ITEM;

    if ($queue_only == TRUE) {
      $queue = $this->queue->get(Connection::QUEUE_RESEND);
      $queue->createItem($element);
      return;
    }

    try {
      // First you need to check if nothing else has unique keys otherwise
      // you find a core bug.
      $response = $this->getHrefLangItem($url, '?filter[path]=' . $values['data']['attributes']['path'] . '', [404]);

      if (isset($values['data']['id']) && !empty($response)) {
        $response = $this->getHrefLangItem($url, '/' . $values['data']['id'], [404]);
      }

      // If old id exist use it.
      if (isset($response['data']['id'])) {
        $values['data']['id'] = $response['data']['id'];
        $options['body'] = json_encode($values);
      }

      if (!empty($response) && isset($response['data']['id'])) {
        $response = $this->updateHrefLangItem($url, $values);
        if (empty($response)) {
          $queue = $this->queue->get(Connection::QUEUE_RESEND);
          $queue->createItem($element);
        }
        return;
      }

      $response = $this->httpClient->request('POST',
        $element['url'] . $this->getResourceAddress(),
        $options
      );

    }
    catch (BadResponseException  $e) {

      $this->handleExceptionResponse($e, [], $url);

      $queue = $this->queue->get(Connection::QUEUE_RESEND);
      $queue->createItem($element);
    }
    catch (\Exception $e) {
      $this->logger->alert(sprintf('POST: %s :: Message: %s', $element['url'] . $this->getResourceAddress(), $e->getMessage()));

      $queue = $this->queue->get(Connection::QUEUE_RESEND);
      $queue->createItem($element);
    }

  }

  /**
   * {@inheritDoc}
   */
  public function createHrefLangItemAsync($url, array $values) {
    /** @var \Drupal\href_lang_exchange\Service\LazyStore $lazy_store */
    $lazy_store = \Drupal::service('href_lang_exchange.service.lazy_store');
    /** @var \Drupal\href_lang_exchange\Service\SettingsManagerInterface $settings_manager */
    $settings_manager = \Drupal::service('href_lang_exchange.helper.settings_manager');
    // You can set it to queue only.
    $lazy_store->addToStore($this, 'createHrefLangItem', [
      $url,
      $values,
      $settings_manager->isClientQueued(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function checkSelfMaster() {
    try {
      $current_host = parse_url(\Drupal::request()->getUri(), PHP_URL_HOST);
      $master_host = parse_url($this->getMaster()->getUrl(), PHP_URL_HOST);
    }
    catch (\Exception $e) {
      $this->logger->info('CheckSelfMaster:: Parse Url:' . $e->getMessage());
      $current_host = 'a';
      $master_host = 'b';
    }
    return strcmp($current_host, $master_host) === 0;
  }

  /**
   * {@inheritDoc}
   */
  public function getMaster() {
    $entity = $this->siteManagement->getMasterSite();

    if (!empty($entity) && $entity instanceof SiteInterface) {
      return $entity;
    }

    throw new \Exception('No master is defined.');
  }

  /**
   * {@inheritDoc}
   */
  public function createHrefLangItemFromEntity(EntityInterface $entity, $gid) {

    if ($entity instanceof HrefLangItemInterface) {
      return $entity;
    }

    $language_id = $entity->language()->getId();
    $country = \Drupal::config('system.date')->get('country.default');

    if (empty($country)) {
      $this->logger->info('Please set a country: /admin/config/regional/settings');
      $country = 'FEHLER';
    }

    $url = $entity->toUrl();
    $url->setAbsolute(TRUE);
    $url->toString();

    /** @var \Drupal\href_lang_exchange\Entity\HrefLangItem $href_lang_item */
    $href_lang_item = HrefLangItem::create([
      'gid' => $gid,
      'region' => $country,
      'language' => $language_id,
      'internal_id' => $this->createInternalId($entity),
      'title' => $entity->getTitle(),
      'path' => $url->toString(),
    ]);

    // It generate a new uuid.
    $href_lang_item->set('uuid', NULL);

    return $href_lang_item;
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceAddress() {
    return Connection::RESOURCE_ADDRESS;
  }

  /**
   * This method create a internal id.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The base entity.
   *
   * @return string
   *   The internal id.
   */
  protected function createInternalId(EntityInterface $entity) {
    $internal_id = $entity->getEntityTypeId() . ':' . $entity->id();
    return $internal_id;
  }

  /**
   * {@inheritDoc}
   */
  public function getAutocompleteItem($filter, $ignore_response = []) {

    $options = CONNECTION::OPTIONS;
    try {

      $url = $this->getMaster()->getUrl();
    }
    catch (\Exception $e) {
      $url = '';
    }

    if (empty($url)) {
      return;
    }

    if ($this->auth->sendAuthHeader()) {
      $options['headers']['Authorization'] = $this->auth->getAuthHeader();
    }

    $element['url'] = $url . $filter;
    $element['options'] = $options;

    $response = NULL;
    try {
      $response = $this->httpClient->get(
        $element['url'],
        $element['options']
      );
    }
    catch (BadResponseException $e) {
      $this->handleExceptionResponse($e, $ignore_response, $url);
    }
    catch (\Exception $e) {
      $this->logger->alert(sprintf('GET: %s :: Message: %s', $element['url'], $e->getMessage()));
      $response = NULL;
    }

    if (!empty($response)) {
      $response = (string) $response->getBody();
      $result = json_decode($response, TRUE);
      if (is_array($result)) {
        return $result;
      }
    }
    return [];
  }

  /**
   * A handler for exception.
   *
   * @param \GuzzleHttp\Exception\RequestException $e
   *   A Guzzle Exception.
   * @param string[] $ignore
   *   Allow to ignore some status codes.
   * @param string $extended_info
   *   Allow to add extended information.
   */
  private function handleExceptionResponse(RequestException $e, array $ignore = [], $extended_info = '') {

    $response = $e->getResponse();
    $response = (string) $response->getBody();
    $response = json_decode($response, TRUE);

    if (isset($response['errors']) && !empty($response['errors'])) {
      foreach ($response['errors'] as $error) {
        if (!in_array($error['status'], $ignore)) {
          if (!empty($e)) {
            $this->logger->alert(sprintf('%s :: %s: %s', $extended_info, $error['title'], $error['detail']));
          }
          else {
            $this->logger->alert(sprintf('%s: %s', $error['title'], $error['detail']));
          }
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityField(EntityInterface $entity, $field_name) {
    if ($entity instanceof FieldableEntityInterface) {
      $definitions = $entity->getFieldDefinitions();

      foreach ($definitions as $definition) {
        if ($definition->getType() === $field_name) {
          return $definition;
        }
      }

    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function pauseSyncTemporary(string $time = '10 minutes') {
    $this->keyValue->set('pause_until', strtotime('+' . $time));
  }

  /**
   * {@inheritDoc}
   */
  public function resumeSync() {
    $this->keyValue->set('pause_until', strtotime('now'));
  }

  /**
   * {@inheritDoc}
   */
  public function isSyncPaused() {
    return (strtotime('now') < $this->keyValue->get('pause_until', strtotime('now') - 1));
  }

}
