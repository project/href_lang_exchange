<?php

namespace Drupal\href_lang_exchange\Connection;

/**
 * Interface Site Management Interface.
 *
 * @package Drupal\href_lang_exchange\Connection
 */
interface SiteManagementInterface {

  /**
   * This method return a list of all destination sites.
   *
   * @return \Drupal\href_lang_exchange\Connection\SiteInterface[]
   *   A list of destination/slave sites.
   */
  public function getAllDestinationSites();

  /**
   * The method  must return the master site.
   *
   * @return \Drupal\href_lang_exchange\Connection\SiteInterface
   *   Return the master sites.
   */
  public function getMasterSite();

}
