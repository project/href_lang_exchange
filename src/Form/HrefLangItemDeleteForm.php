<?php

namespace Drupal\href_lang_exchange\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a HrefLangItem entity.
 */
class HrefLangItemDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this item?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $url = new Url('view.href_lang_item_list.list');
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entityId = $this->getEntity()->id();
    $this->getEntity()->delete();
    $this->logger('href_lang_exchange')->notice('Deleted HreflangItem with id %id', ['%id' => $entityId]);
    $form_state->setRedirect('view.href_lang_item_list.list');
  }

}
