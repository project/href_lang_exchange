<?php

namespace Drupal\href_lang_exchange\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;

/**
 * Filters by href lang item.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("filter_href_lang")
 */
class FilterByHrefLangItems extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * Op for no group.
   */
  public function opNoGroup() {
    $this->ensureMyTable();

    $items = \Drupal::entityTypeManager()
      ->getStorage('href_lang_item')
      ->getAllUnconnectedItems();

    $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", array_values($items), 'IN');

  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   *
   * @return array
   *   Return a list of possible options.
   */
  public function generateOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();
    $operators['only_single'] = [
      'title' => $this->t('Has no group'),
      'short' => $this->t('no_group'),
      'short_single' => $this->t('NG'),
      'method' => 'opNoGroup',
      'values' => 0,
    ];

    return $operators;
  }

}
