<?php

namespace Drupal\href_lang_exchange\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "CheckSiteIDExist",
 *   label = @Translation("Check if the master href instances is available", context = "Validation"),
 *   type = "string"
 * )
 */
class CheckSiteIDExist extends Constraint {

  /**
   * Notification text.
   *
   * @var string
   */
  public $notInteger = 'Please add a Site ID for the href exchange';

}
