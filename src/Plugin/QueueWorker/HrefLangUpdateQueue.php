<?php

namespace Drupal\href_lang_exchange\Plugin\QueueWorker;

use Psr\Log\LoggerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Tasks for Learning.
 *
 * @QueueWorker(
 *   id = "resend_creation_href_lang_queue",
 *   title = @Translation("resend href post request"),
 *   cron = {"time" = 60}
 * )
 */
class HrefLangUpdateQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  const CREATE_LANG_ITEM = 1;
  const DELETE_LANG_ITEM = 2;

  /**
   * The Psr Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Connection.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('href_lang_exchange.connection'),
      $container->get('logger.factory')->get('href_lang_exchange')
    );
  }

  /**
   * Constructs a HrefLangUpdateQueue object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\href_lang_exchange\Connection\ConnectionInterface $connection
   *   The connection Object.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConnectionInterface $connection, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (
      (!isset($data['url'])) ||
      (!isset($data['values'])) ||
      (!isset($data['type']))
    ) {
      $this->logger
        ->critical('Not well formed href lang request', $data);
    }

    switch ($data['type']) {
      case HrefLangUpdateQueue::CREATE_LANG_ITEM:
        $this->connection->createHrefLangItem($data['url'], $data['values']);
        break;

      case HrefLangUpdateQueue::DELETE_LANG_ITEM:
        $this->connection->deleteHrefLangItem($data['url'], $data['values']);
        break;
    }
  }

}
