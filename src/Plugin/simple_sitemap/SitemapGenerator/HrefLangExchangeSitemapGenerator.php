<?php

namespace Drupal\href_lang_exchange\Plugin\simple_sitemap\SitemapGenerator;

use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator\DefaultSitemapGenerator;

/**
 * HrefLang Exchange Sitemap Generator.
 *
 * @package Drupal\href_lang_exchange\Plugin\simple_sitemap\HrefLangExchangeSitemapGenerator
 *
 * @SitemapGenerator(
 *   id = "href_lang_exchange",
 *   label = @Translation("Default sitemap generator for href lang exchange"),
 *   description = @Translation("Generates a standard conform hreflang sitemap
 *   of your content."),
 * )
 */
class HrefLangExchangeSitemapGenerator extends DefaultSitemapGenerator {

  /**
   * Checks if sitemap is hreflang compliant.
   *
   * @return bool
   *   Return true if it a hreflang sitemap.
   */
  protected function isHreflangSitemap() {
    return TRUE;
  }

}
