<?php

namespace Drupal\href_lang_exchange\Plugin\simple_sitemap\SitemapType;

use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapType\DefaultHreflangSitemapType;

/**
 * Integration with sitemap.
 *
 * @package Drupal\href_lang_exchange\Plugin\simple_sitemap\HrefLangExchangeSitemapType
 *
 * @SitemapType(
 *   id = "default_href_lang_exchange",
 *   label = @Translation("Default href_lang_exchange"),
 *   description = @Translation("The exchange hreflang sitemap type."),
 *   sitemapGenerator = "href_lang_exchange",
 *   urlGenerators = {
 *     "custom",
 *     "entity",
 *     "entity_menu_link_content",
 *     "arbitrary",
 *   },
 * )
 */
class HrefLangExchangeSitemapType extends DefaultHreflangSitemapType {
}
