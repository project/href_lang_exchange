<?php
// @codingStandardsIgnoreFile

namespace Drupal\href_lang_exchange\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Session\AccountInterface;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;
use Drupal\href_lang_exchange\HrefLangItemStorage;
use Drupal\href_lang_exchange\Service\HrefLangItemValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "href_lang_exchange_item_autocomplete",
 *   label = @Translation("Autocomplete for Href"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "href_lang_exchange_item"
 *   }
 * )
 */
class HrefLangExchangeFieldItemAutocompleteWidget extends StringTextfieldWidget implements ContainerFactoryPluginInterface {

  /**
   * The storage for HrefLangItem entities.
   *
   * @var \Drupal\href_lang_exchange\HrefLangItemStorage
   */
  protected $storage;

  /**
   * The connection object.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $connection;

  /**
   * The validator object.
   *
   * @var \Drupal\href_lang_exchange\Service\HrefLangItemValidatorInterface
   */
  protected $validator;

  /**
   * {@inheritDoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, HrefLangItemStorage $storage, ConnectionInterface $connection, HrefLangItemValidatorInterface $validator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->storage = $storage;
    $this->connection = $connection;
    $this->validator = $validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('href_lang_item'),
      $container->get('href_lang_exchange.connection'),
      $container->get('href_lang_exchange.service.href_lang_item_validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $this->loadEntity($items, $form_state);
    $language_id = $entity->language()->getId();

    $default_value_textfield = $entity->get($items->getFieldDefinition()->getName())->getString();

    // For the validation.
    $form_state->set('entity', $entity);

    $text_max_length = 60;

    try {
      $autocomplet_url = $this->connection->getMaster()
        ->getUrl() . '/href_lang_autocomplete';

    }
    catch (\Exception $e) {
      $autocomplet_url = 'No_Master_is_set';
    }

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? $default_value_textfield,
      '#size' => $this->getSetting('size'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#attributes' => [
        'class' => [
          'hide',
          'js-text-full',
          'text-full',
          'href_lang_gid_value',
        ],
      ],
      '#element_validate' => [[$this, 'validate']],
      '#attached' => [
        'library' => ['href_lang_exchange/href_lang_exchange_autocomplete'],
        'drupalSettings' => [
          'href_lang_exchange' => [
            'autocomplet_adress' => $autocomplet_url,
            'text_max_length' => $text_max_length,
            'sub_lang' => $language_id,
            'site' => $_SERVER['HTTP_HOST'],
            'ignore_elements' => $items[$delta]->value,
          ],
        ],
      ],
    ];

    $languages = $this->getPossibleOptions();

    $element['search_language_filter'] = [
      '#type' => 'select',
      '#title' => t('Language'),
      '#title_display' => 'invisible',
      '#attributes' => [
        'class' => [
          'js-text-full',
          'text-full',
          'autocomplete_href_lang_filter_lang',
        ],
      ],
      '#options' => ['' => $this->t('- None -')] + $languages,
    ];

    $element['search_field'] = [
      '#type' => 'textfield',
      '#title' => t('Search'),
      '#title_display' => 'invisible',
      '#attributes' => [
        'class' => [
          'js-text-full',
          'text-full',
          'autocomplete_href_lang',
        ],
      ],
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $group_href = [];

    if (isset($items[$delta]->value)) {
      RenderElement::setAttributes($element['search_language_filter'], ['search_off']);
      RenderElement::setAttributes($element['search_field'], ['search_off']);

      $group_href = $this->storage->loadAllByGid($items[$delta]->value);
    }

    $element['preview'] = [
      '#theme' => 'href_lang_preview',
      '#first' => array_shift($group_href),
      '#is_gid_set' => isset($items[$delta]->value),
      '#entities' => $group_href,
      '#text_max_length' => $text_max_length,
    ];

    $element['value']['#title_display'] = 'invisible';

    return $element;
  }

  /**
   * Load the current active entity (Both entities have different information).
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The FieldItemList object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|\Drupal\Core\Entity\TranslatableInterface
   *   Return the current active entity.
   */
  private function loadEntity(FieldItemListInterface $items, FormStateInterface $form_state) {
    $entity = $items->getEntity();

    if ($entity instanceof TranslatableInterface) {

      $storage = $form_state->getStorage();

      if (
        isset($storage['langcode']) &&
        isset($storage['entity_default_langcode']) &&
        $storage['entity_default_langcode'] != $storage['langcode']
      ) {
        $entity = $entity->getTranslation($storage['langcode']);
      }

    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL, $format = 'en') {
    $select_options = [];

    // No need to cache this data. It is a hardcoded list.
    $languages = \Drupal::languageManager()->getStandardLanguageList();

    \Drupal::moduleHandler()->alter('alter_href_lang_list', $languages);

    // Format the array to Options format.
    foreach ($languages as $langcode => $language_names) {
      $language_name = '';
      switch ($format) {
        case 'en':
          $language_name .= $this->t("{$language_names[0]}");
          break;

        case 'loc':
          $language_name .= $language_names[1];
          break;

        case 'both':
          $language_name .= $this->t("{$language_names[0]}");
          if (mb_strlen($language_names[1])) {
            $language_name .= ' (' . $language_names[1] . ')';
          }
          $language_name .= ' [' . $langcode . ']';
          break;
      }

      $select_options[$langcode] = $language_name;
    }

    \Drupal::moduleHandler()->alter('possible_language_options', $select_options);
    // @todo separate module.
    $field_region = 'field_country';
    $new_list = [];

    /** @var \Drupal\href_lang_exchange_href\SiteEntityStorageInterface $site_storage */
    $site_storage = \Drupal::entityTypeManager()
      ->getStorage('site_entity');

    $list_of_languages = $site_storage->getAggregateQuery()
      ->groupBy($field_region)
      ->aggregate($field_region, 'COUNT')
      ->execute();

    foreach ($list_of_languages as $item) {
      if (isset($select_options[strtolower($item[$field_region])])) {
        $new_list[strtolower($item[$field_region])] = $select_options[strtolower($item[$field_region])];
      }
    }

    $select_options = $new_list;

    asort($select_options);

    return $select_options;
  }

  /**
   * Validation method for the HrefLangFormWidget.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   table element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validate(array $element, FormStateInterface $form_state, array &$complete_form) {

    $path = $form_state->getStorage();
    /* EntityInterface $entity*/
    $entity = $path['entity'];
    // @todo check validation
    // $this->validator->validateByUniqueTypes($entity, $form_state, $element);.
  }

}
