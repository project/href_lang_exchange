<?php

namespace Drupal\href_lang_exchange\Plugin\Field\FieldType;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Language\Language;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Provides a field type of href lang exchange item.
 *
 * Injection is currently not working.
 *
 * @see https://www.drupal.org/node/2053415
 *
 * @FieldType(
 *   id = "href_lang_exchange_item",
 *   label = @Translation("Href lang exchange item"),
 *   description = @Translation("A field containing"),
 *   category = @Translation("Reference"),
 *   default_widget = "href_lang_exchange_item_autocomplete",
 *   default_formatter = "href_lang_exchange_item_formatter",
 *   constraints = {"CheckSiteIDExist" = {}},
 * )
 */
class HrefLangExchangeFieldItem extends StringItem {

  /**
   * The connection object.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $connection;

  /**
   * The Psr Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $generator;

  /**
   * {@inheritDoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);

    $this->connection = \Drupal::service('href_lang_exchange.connection');
    $this->logger = \Drupal::logger('href_lang_exchange');
    $this->generator = \Drupal::service('uuid');
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if (empty($this->value)) {
      $this->value = $this->generator->generate();
    }
  }

  /**
   * You don't get the right translation normaly.....
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|mixed
   *   The current entity.
   */
  private function loadEntity() {
    $parameter_bag = \Drupal::request()->attributes;
    $language = NULL;

    // Only the entity from this->getEntity has a original ....
    foreach ($parameter_bag as $item) {
      if ($parameter_bag->has('target') && $parameter_bag->get('target') instanceof Language) {
        $language = $parameter_bag->get('target');
      }

      if ($item instanceof ContentEntityInterface) {
        if ($language instanceof Language) {
          return $item->getTranslation($language->getId());
        }
        return $item;
      }
    }

    return $this->getEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {

    $entity = $this->loadEntity();

    if (
      $entity instanceof EntityPublishedInterface &&
      !$entity->isPublished()
    ) {
      return;
    }

    try {
      $master = $this->connection->getMaster();
      $master_url = $master->getUrl();
      $href_lang_item = $this->connection->createHrefLangItemFromEntity($this->getEntity()->getTranslation($entity->language()->getId()), $this->getValue());

      $item = $href_lang_item->toJsonApi();

      /** @var \Drupal\Core\Entity\EntityInterface $original */
      $original = $this->getEntity()->original;

      // If you translate something the original is the untranslated entity.
      if (
        !empty($original) &&
        $original->hasTranslation($entity->language()->getId())
      ) {
        $original = $original->getTranslation($entity->language()->getId());
      }

      if (
        !empty($original) &&
        $original->language()->getId() === $entity->language()->getId()
      ) {

        $url = $original->toUrl();
        $url->setAbsolute(TRUE);
        $url = $url->toString();

        $response = $this->connection->getHrefLangItem($master_url, '?filter[path]=' . $url . '', [404]);
        if (!empty($response) && isset($response['data']['id'])) {
          $item['data']['id'] = $response[0]['data']['id'];
        }
        // If we have multiple values.
        if (!empty($response['data']) && is_array($response['data'])) {
          $item['data']['id'] = $response['data'][0]['id'];
        }
      }

      // Switch between queue and directly [queue-it].
      $this->connection->createHrefLangItemAsync($master_url, $item);

    }
    catch (\Exception $e) {
      $this->logger->alert("Can't save Field: " . $e->getMessage());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    $entity = $this->getEntity();
    $languages = [];

    try {

      $master = $this->connection->getMaster();
      $master_url = $master->getUrl();

      $href_lang_item = $this->connection->createHrefLangItemFromEntity($entity, $this->getString());

      // The url alias is already deleted at this time.
      $response = $this->connection->getHrefLangItem($master_url, '?filter[gid]=' . $this->getString() . '', [404]);

      if (isset($response['data']) && is_array($response['data'])) {
        $languages[$href_lang_item->getLanguage()] = $href_lang_item->getLanguage();
        if ($entity instanceof TranslatableInterface) {
          $languages = $entity->getTranslationLanguages();
          foreach ($languages as $language) {
            $languages[$language->getId()] = $language->getId();
          }
        }

        $response['data'] = array_filter($response['data'], function ($element) use ($href_lang_item, $languages) {
          // Maybe filter path.
          if (
          in_array($element['attributes']['language'], $languages)
          ) {
            return TRUE;
          }
          return FALSE;
        });

        foreach ($response['data'] as $element) {
          if (isset($element['id'])) {
            $href_lang_item->set('uuid', $element['id']);
            $this->connection->deleteHrefLangItemAsync($master_url, $href_lang_item->toJsonApi());
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->alert('delete() HrefLangExchangeFieldItem:' . $e->getMessage());
    }

  }

}
