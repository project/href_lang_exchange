<?php

namespace Drupal\href_lang_exchange\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\href_lang_exchange\HrefLangItemState;
use Drupal\href_lang_exchange\HrefLangItemStorage;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "href_lang_exchange_item_formatter",
 *   label = @Translation("Render HTML Tags"),
 *   field_types = {
 *     "href_lang_exchange_item"
 *   }
 * )
 */
class HrefLangExchangeFieldItemFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The Psr Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The href lang item storage.
   *
   * @var \Drupal\href_lang_exchange\HrefLangItemStorage
   */
  protected $storage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Add the href html tags.');
    return $summary;
  }

  /**
   * {@inheritDoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, HrefLangItemStorage $storage, LoggerInterface $logger, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->storage = $storage;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('href_lang_item'),
      $container->get('logger.channel.href_lang_exchange'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $head_elememts = [];

    $entity = $items->getEntity();

    try {
      $url = $entity->toUrl();
      $url->setAbsolute(TRUE);
      $url = $url->toString();
    }
    catch (\Exception $e) {
      $this->logger->info("Can't create url :" . $e->getMessage());
      $url = '';
    }

    foreach ($items as $delta => $item) {
      $this->addMetatags($item->value, $head_elememts, []);
    }

    if (!empty($head_elememts)) {
      $element['#attached']['html_head'] = $head_elememts;
    }

    return $element;
  }

  /**
   * Add Metatags to a given Array.
   *
   * @param string $value
   *   The gid value.
   * @param array $html_head
   *   The array for the html head.
   * @param array $exclude
   *   This (optional) exclude this urls.
   */
  private function addMetatags($value, array &$html_head, array $exclude = []) {

    $entities = $this->storage->loadAllByGid($value, HrefLangItemState::PUBLISHED);

    foreach ($entities as $entity) {

      if (in_array($entity->getPath(), $exclude)) {
        continue;
      }

      $href_lang = strtolower($entity->getLanguage());

      if ($entity->getLanguage() != $entity->getRegion()) {
        $href_lang = strtolower($entity->getLanguage()) . '-' . strtoupper($entity->getRegion());
      }

      $tag = [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'alternate',
          'href' => $entity->getPath(),
          'hreflang' => $href_lang,
        ],
      ];
      $key = 'hreflang' . $href_lang;

      $this->moduleHandler->alter(
        'href_lang_head_links',
        $entity,
        $tag,
        $key
      );

      $html_head[] = [$tag, 'hreflang' . $key];
    }

  }

}
