<?php

namespace Drupal\href_lang_exchange\Plugin\Block;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a href connection count block.
 *
 * @Block(
 *   id = "href_language_connection_count_block",
 *   admin_label = @Translation("href lang item connection count"),
 *   category = @Translation("System"),
 * )
 */
class HrefLanguageConnectionCountBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The href lang storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $storage;


  /**
   * The route matcher.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $storage, EntityFieldManagerInterface $field_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->storage = $storage;
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entitiy = NULL;

    $list = $this->createSearchList('href_lang_exchange_item');

    $list_of_href_lang_item = $this->storage->getStorage('href_lang_item');
    $query_groups = $list_of_href_lang_item->getAllUnconnectedItems();

    foreach ($list as $entity_name => $entity_infos) {
      $entity_storage = $this->storage->getStorage($entity_name);

      $entity_bundles = $entity_infos['bundles'] ?? [];
      $field_name = $entity_infos['field_name'] ?? '';

      foreach ($entity_bundles as $bundle) {
        $all_items = $entity_storage->getAggregateQuery()
          ->aggregate('uuid', 'COUNT')
          ->condition('type', $bundle)
          ->execute();

        $only_items_with_group = $entity_storage->getAggregateQuery()
          ->aggregate('uuid', 'COUNT')
          ->condition('type', $bundle)
          ->condition($field_name, $query_groups, 'IN')
          ->exists($field_name)
          ->execute();

        $info[$entity_name][$bundle]['all'] = $all_items[0]['uuid_count'] ?? 0;
        $info[$entity_name][$bundle]['no_group'] = $only_items_with_group[0]['uuid_count'] ?? 0;
        $info[$entity_name][$bundle]['total_percentage'] = 0;
        if ($info[$entity_name][$bundle]['all'] > 0) {
          $info[$entity_name][$bundle]['total_percentage'] =
            (($info[$entity_name][$bundle]['all'] - $info[$entity_name][$bundle]['no_group']) / $info[$entity_name][$bundle]['all']) * 100;
        }
      }
    }
    return [
      [
        '#markup' => Markup::create('<style> .graph-cont .bar42::after {    max-width:' . $info[$entity_name][$bundle]['total_percentage'] . '%;}</style>'),
        '#attached' => [
          'library' => ['href_lang_exchange/href_lang_exchange_bar_style'],
        ],
      ],
      [
        '#theme' => 'href_lang_connection_block',
        '#info' => $info,
        '#cache' => [
          'tags' => [
            'language_switch',
          ],
          'contexts' => [
            'url',
          ],
        ],
      ],
    ];
  }

  /**
   * Create a list of all entities and bundles with a specific Field.
   *
   * @param string $field_name
   *   Filter by this field name.
   *
   * @return array
   *   List of Entities and Bundles with a field of type $field_name.
   */
  private function createSearchList($field_name) {
    $list = [];
    $definitions = $this->fieldManager->getFieldMap();
    foreach ($definitions as $entity_name => $definition) {
      foreach ($definition as $field => $values) {
        $type = $values['type'] ?? '';
        if ($type === $field_name) {
          $bundles = $values['bundles'] ?? '';
          $list[$entity_name] = [
            'field_name' => $field,
            'bundles' => $bundles,
          ];
        }
      }
    }
    return $list;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_route_match')
    );
  }

}
