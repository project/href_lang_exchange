<?php

namespace Drupal\href_lang_exchange\Plugin\Block;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\href_lang_exchange\HrefLangItemState;
use Drupal\href_lang_exchange\HrefLangItemStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a href language switch block.
 *
 * @Block(
 *   id = "href_language_switch_block",
 *   admin_label = @Translation("href lang item language switcher"),
 *   category = @Translation("Language switcher"),
 * )
 */
class HrefLanguageSwitchBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The href lang storage.
   *
   * @var \Drupal\href_lang_exchange\HrefLangItemStorageInterface
   */
  protected $storage;


  /**
   * The route matcher.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HrefLangItemStorageInterface $storage, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entitiy = NULL;

    foreach ($this->routeMatch->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $entitiy = $param;
        break;
      }
    }

    if (empty($entitiy)) {
      return [
        '#theme' => 'href_lang_switch_language_block',
        '#entities' => [],
      ];
    }

    $internal_id = $entitiy->getEntityTypeId() . ':' . $entitiy->id();

    $href_lang_entity = $this->storage->loadByInternalId($internal_id);

    if (!empty($href_lang_entity)) {
      $entities = $this->storage->loadAllByGid($href_lang_entity->getGid(), HrefLangItemState::PUBLISHED);
    }

    return [
      '#theme' => 'href_lang_switch_language_block',
      '#entities' => $entities,
      '#cache' => [
        'tags' => [
          'language_switch',
        ],
        'contexts' => [
          'url',
        ],
      ],
    ];
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('href_lang_item'),
      $container->get('current_route_match')
    );
  }

}
