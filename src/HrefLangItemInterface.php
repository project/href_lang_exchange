<?php

namespace Drupal\href_lang_exchange;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Href lang item entity.
 *
 * @ingroup content_entity_example
 */
interface HrefLangItemInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Return the langcode.
   *
   * @return string
   *   The Langcode.
   */
  public function getLanguage();

  /**
   * Return the group ID.
   *
   * @return string
   *   The Group ID.
   */
  public function getGid();

  /**
   * Return the url.
   *
   * @return string
   *   The Url.
   */
  public function getUrl();

  /**
   * Convert entity to jsonApi php array.
   *
   * @return array
   *   The jsonApi php array.
   */
  public function toJsonApi();

  /**
   * Return the region.
   *
   * @return string
   *   The region.
   */
  public function getRegion();

  /**
   * Return the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle();

  /**
   * Return the internal id .
   *
   * @return string
   *   The internal id.
   */
  public function getInternalId();

  /**
   * Method to set the region value.
   *
   * @param string $region
   *   A region key.
   *
   * @return \Drupal\href_lang_exchange\HrefLangItemInterface
   *   Return the own href lang item.
   */
  public function setRegion($region);

  /**
   * Method to set the language value.
   *
   * @param string $language
   *   A language key.
   *
   * @return \Drupal\href_lang_exchange\HrefLangItemInterface
   *   Return the own href lang item.
   */
  public function setLanguage($language);

  /**
   * Set the path.
   *
   * @param string $path
   *   The url.
   */
  public function setPath($path);

  /**
   * Set the gid.
   *
   * @param string $gid
   *   The Group ID.
   */
  public function setGid($gid);

}
