<?php

namespace Drupal\href_lang_exchange\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\href_lang_exchange\Service\LazyStoreInterface;
use Drupal\href_lang_exchange\Service\LazyStoreItemJsonExtensionInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Lazy Href Item Subscriber.
 *
 * @package Drupal\href_lang_exchange\EventSubscriber
 */
class LazyHrefItemSubscriber implements EventSubscriberInterface {

  /**
   * The lazy store.
   *
   * @var \Drupal\href_lang_exchange\Service\LazyStoreInterface
   */
  protected $lazyStore;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * LazyHrefItemSubscriber constructor.
   *
   * @param \Drupal\href_lang_exchange\Service\LazyStoreInterface $lazy_store
   *   The lazy store.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger.
   */
  public function __construct(LazyStoreInterface $lazy_store, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactory $logger) {
    $this->lazyStore = $lazy_store;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger->get('href_lang_exchange');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::TERMINATE => 'lazyHrefTerminate',
    ];
  }

  /**
   * React on Terminate and execute open Task.
   */
  public function lazyHrefTerminate() {
    $services = $this->lazyStore->getStore();

    foreach ($services as $service) {

      if (
        $service instanceof LazyStoreItemJsonExtensionInterface
      ) {
        $ids = explode(':', $service->getInternalId());
        if (count($ids) == 2) {
          try {
            $entity = $this->entityTypeManager
              ->getStorage($ids[0])
              ->load($ids[1]);
            if (!empty($entity)) {
              $entity = $entity->getTranslation($service->getLangcode());
              if (!empty($entity) && $entity instanceof EntityInterface) {
                $url = $entity->toUrl();
                $url->setAbsolute(TRUE);
                $url_string = $url->toString();
                $service->setPath($url_string);
              }
            }
          }
          catch (\Exception $e) {
            $this->logger->notice(sprintf("Can't load entity %s:%s", $ids[0], $ids[1]));
          }
        }
      }

      call_user_func_array([
        $service->getObject(),
        $service->getMethod(),
      ], $service->getAttributes());
    }
  }

}
