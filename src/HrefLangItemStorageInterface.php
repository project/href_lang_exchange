<?php

namespace Drupal\href_lang_exchange;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for href lang item entities.
 *
 * This extends the base storage class, adding required special handling for
 * Href lang item entities.
 */
interface HrefLangItemStorageInterface extends ContentEntityStorageInterface {

  /**
   * Load the first match of the internal id.
   *
   * @param string $id
   *   A internal id.
   *
   * @return \Drupal\href_lang_exchange\Entity\HrefLangItem
   *   A HrefLangItem.
   */
  public function loadByInternalId($id);

  /**
   * Load every match by the internal id.
   *
   * @param string $id
   *   A internal id.
   *
   * @return \Drupal\href_lang_exchange\Entity\HrefLangItem[]
   *   A list of HrefLangItems.
   */
  public function loadAllByInternalId($id);

  /**
   * Load all HrefLangItem that has the gid.
   *
   * @param string $gid
   *   A internal id.
   * @param string $status
   *   A internal id.
   *
   * @return \Drupal\href_lang_exchange\Entity\HrefLangItem[]
   *   An array of HrefLangItem entities.
   */
  public function loadAllByGid($gid, $status = '');

  /**
   * Load all HrefLangItem that has the path.
   *
   * @return \Drupal\href_lang_exchange\Entity\HrefLangItem[]
   *   An array of HrefLangItem entities.
   */
  public function loadAllByPath($path);

  /**
   * Load all gid by region and language.
   *
   * @param string $region
   *   The regioncode.
   * @param string $language
   *   The langcode.
   *
   * @return string[]
   *   A list of all matching group ids.
   */
  public function getAllGidByRegionAndLanguage($region, $language);

  /**
   * Load all langcodes.
   *
   * @return string[]
   *   A list of all languages.
   */
  public function loadAllLanguages();

  /**
   * Load all unconnected items.
   *
   * @return array
   *   Return a list of unconnected gid.
   */
  public function getAllUnconnectedItems();

}
