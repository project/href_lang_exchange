<?php

namespace Drupal\href_lang_exchange;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the HrefLangItem entity.
 *
 * @see \Drupal\href_lang_exchange\Entity\HrefLangItem.
 */
class HrefLangItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view href_lang_item entity');

      case 'update':
      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit href_lang_item entity');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete href_lang_item entity');
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   *
   * Separate from the checkAccess because the entity does not yet exist, it
   * will be created during the 'add' process.
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add href_lang_item entity');
  }

}
