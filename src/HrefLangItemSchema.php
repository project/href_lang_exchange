<?php

namespace Drupal\href_lang_exchange;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Class Href Lang Item Schema.
 *
 * @package Drupal\href_lang_exchange
 */
class HrefLangItemSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    // Normally it is not necessary https://www.drupal.org/node/2858195 but ....
    $aSchema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $names = ['language', 'gid'];
    if ($table_name === 'href_lang_item' && in_array($storage_definition->getName(), $names)) {
      $aSchema['fields'][$storage_definition->getName()]['not null'] = TRUE;
    }
    return $aSchema;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeBaseTable(ContentEntityTypeInterface $entity_type) {
    $schema_table = parent::initializeBaseTable($entity_type);
    $schema_table['unique keys'] = [
      'only_one' => ['gid', 'region', 'language'],
    ];

    return $schema_table;
  }

}
