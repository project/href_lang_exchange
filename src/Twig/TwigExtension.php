<?php

namespace Drupal\href_lang_exchange\Twig;

/**
 * Class Twig Extension.
 *
 * @package Drupal\href_lang_exchange\Twig
 */
class TwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('t_langcode', [$this, 'getLangcodeLabel']),
      new \Twig_SimpleFilter('t_region', [$this, 'getRegionLabel']),
    ];
  }

  /**
   * Wrapper around render() for twig printed output.
   *
   * If an object is passed which does not implement __toString(),
   * RenderableInterface or toString() then an exception is thrown;
   * Other objects are casted to string. However in the case that the
   * object is an instance of a Twig_Markup object it is returned directly
   * to support auto escaping.
   *
   * If an array is passed it is rendered via render() and scalar values are
   * returned directly.
   *
   * @param mixed $arg
   *   String, Object or Render Array.
   *
   * @return mixed
   *   The rendered output or an Twig_Markup object.
   *
   * @throws \Exception
   *   When $arg is passed as an object which does not implement __toString(),
   *   RenderableInterface or toString().
   *
   * @see render
   * @see TwigNodeVisitor
   */
  public function getRegionLabel($arg) {
    // Check for a numeric zero int or float.
    if ($arg === 0 || $arg === 0.0) {
      return 0;
    }

    // Return early for NULL and empty arrays.
    if ($arg == NULL) {
      return NULL;
    }
    // Optimize for scalars as it is likely they come from the escape filter.
    if (is_numeric($arg)) {
      return $arg;
    }

    if (is_string($arg)) {
      $countries = \Drupal::service('country_manager')->getList();
      if (isset($countries[$arg])) {
        return $countries[$arg];
      }
    }

    return NULL;
  }

  /**
   * Wrapper around render() for twig printed output.
   *
   * If an object is passed which does not implement __toString(),
   * RenderableInterface or toString() then an exception is thrown;
   * Other objects are casted to string. However in the case that the
   * object is an instance of a Twig_Markup object it is returned directly
   * to support auto escaping.
   *
   * If an array is passed it is rendered via render() and scalar values are
   * returned directly.
   *
   * @param mixed $arg
   *   String, Object or Render Array.
   *
   * @return mixed
   *   The rendered output or an Twig_Markup object.
   *
   * @throws \Exception
   *   When $arg is passed as an object which does not implement __toString(),
   *   RenderableInterface or toString().
   *
   * @see render
   * @see TwigNodeVisitor
   */
  public function getLangcodeLabel($arg) {
    // Check for a numeric zero int or float.
    if ($arg === 0 || $arg === 0.0) {
      return 0;
    }

    // Return early for NULL and empty arrays.
    if ($arg == NULL) {
      return NULL;
    }

    // Optimize for scalars as it is likely they come from the escape filter.
    if (is_scalar($arg)) {
      return $arg;
    }

    if (is_string($arg)) {
      $languages = \Drupal::languageManager()->getStandardLanguageList();
      if (isset($languages[$arg])) {
        return $languages[$arg][0];
      }
    }

    return NULL;
  }

}
