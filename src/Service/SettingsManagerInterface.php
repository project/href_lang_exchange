<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Interface Settings Manager service.
 *
 * @package Drupal\href_lang_exchange\Service
 */
interface SettingsManagerInterface {

  /**
   * Check if we should queue the admin requests.
   *
   * @return bool
   *   True if it should be queued else false.
   */
  public function isAdminQueued();

  /**
   * Check if we should queue the client request.
   *
   * @return bool
   *   True if it should be queued else false.
   */
  public function isClientQueued();

}
