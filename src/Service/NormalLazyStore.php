<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Provide NormalLazy load service.
 *
 * @package Drupal\href_lang_exchange\Service
 */
class NormalLazyStore implements LazyStoreItemInterface {

  /**
   * This is the object for the method.
   *
   * @var object
   */
  protected $object;

  /**
   * This is the name of the method that should be invoked.
   *
   * @var string
   */
  protected $method;

  /**
   * This a the parameter/attributes to invoke the method.
   *
   * @var array
   */
  protected $attributes;

  /**
   * {@inheritdoc}
   */
  public function __construct($object, $method, array $attributes) {
    $this->object = $object;
    $this->method = $method;
    $this->attributes = $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getObject() {
    return $this->object;
  }

  /**
   * {@inheritdoc}
   */
  public function setObject($object) {
    $this->object = $object;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * {@inheritdoc}
   */
  public function setMethod($method) {
    $this->method = $method;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttributes() {
    return $this->attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function setAttributes(array $attributes) {
    $this->attributes = $attributes;
  }

}
