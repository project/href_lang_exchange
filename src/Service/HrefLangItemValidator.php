<?php

namespace Drupal\href_lang_exchange\Service;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class Href Lang Item Validator.
 *
 * @package Drupal\href_lang_exchange\Service
 */
class HrefLangItemValidator implements HrefLangItemValidatorInterface {

  use StringTranslationTrait;

  /**
   * The connection object.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $connection;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * HrefLangItemValidator constructor.
   *
   * @param \Drupal\href_lang_exchange\Connection\ConnectionInterface $connection
   *   The connection object.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(ConnectionInterface $connection, LoggerInterface $logger) {
    $this->connection = $connection;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function validateByUniqueTypes(EntityInterface $entity, FormStateInterface $form_state, array $element) {
    // $form_state and $entity old input new;
    $user_input = $form_state->getUserInput();

    $path_current = '';
    if (isset($user_input['path'][0]['value'])) {
      $path_current = $user_input['path'][0]['value'];
    }

    $language = $form_state->getValue('language', $form_state->getValue('langcode', [['value' => 'en']]));
    $language = $language[0]['value'];

    // If you create it manual you can set a different country.
    $region = $form_state->getValue('region', [
      [
        'value' => $region = \Drupal::config('system.date')
          ->get('country.default'),
      ],
    ]);

    $region = $region[0]['value'];

    $gid = $form_state->getValue('gid')[0]['value'];

    if (empty($gid) && isset($element['#value'])) {
      $gid = $element['#value'];
    }

    try {

      $check = $this->connection->getHrefLangItem($this->connection->getMaster()
        ->getUrl(), '?filter[language]=' . $language . '&filter[region]=' . $region . '&filter[gid]=' . $gid);
    }
    catch (\Exception $e) {
      $form_state->setError($element, $this->t('No master server is available.'));

    }

    if (is_array($check) && !empty($check)) {

      $value = $check['data']['attributes'];

      if ($entity->isNew()) {
        $form_state->setError($element, t('For the Langcode: @langcode , Region: @region and GID: @gid exist the Entry: @path (UUID: @uuid  )', [
          '@langcode' => $value['language'],
          '@region' => $value['region'],
          '@gid' => $value['gid'],
          '@path' => $value['path'],
          '@uuid' => $check['data']['id'],
        ]));
      }

      try {
        $href_lang_item = $this->connection->createHrefLangItemFromEntity($entity, $gid);
        if (empty($path_current)) {
          $path_current = $href_lang_item->getUrl();
        }

        // Maybe remove the gid check and find a better solution.
        if (
          ($check['data']['attributes']['path'] != $path_current &&
            $check['data']['id'] != $href_lang_item->uuid()) &&
          $check['data']['attributes']['gid'] != $href_lang_item->getGid()
        ) {
          $form_state->setError($element, t('For the Langcode: @langcode , Region: @region and GID: @gid exist the Entry: @path (UUID: @uuid  )', [
            '@langcode' => $value['language'],
            '@region' => $value['region'],
            '@gid' => $value['gid'],
            '@path' => $value['path'],
            '@uuid' => $check['data']['id'],
          ]));
        }

      }
      catch (EntityMalformedException $em) {
        $form_state->setError($element, $this->t("Can\'t create a href lang entity."));
        $this->logger->error('Can\'t create a href lang entity.' . $em->getMessage());

      }
    }
    if (!is_array($check) && $check == NULL) {
      $form_state->setError($element, $this->t('No master server is available.'));
      $this->logger->error('No master server is available.');
    }
  }

}
