<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Interface Lazy Store Item Interface.
 *
 * @package Drupal\href_lang_exchange\Service
 */
interface LazyStoreItemInterface {

  /**
   * Get the object.
   *
   * @return object
   *   Return a object.
   */
  public function getObject();

  /**
   * Set the object.
   *
   * @param object $object
   *   Return the object where the method should invoked on.
   */
  public function setObject($object);

  /**
   * Get the method.
   *
   * @return string
   *   The method name.
   */
  public function getMethod();

  /**
   * Set the method.
   *
   * @param string $method
   *   The method name.
   */
  public function setMethod($method);

  /**
   * Return the attributes.
   *
   * @return array
   *   A array with parameters/attributes.
   */
  public function getAttributes();

  /**
   * Set the attributes.
   *
   * @param array $attributes
   *   A array of attributes.
   */
  public function setAttributes(array $attributes);

}
