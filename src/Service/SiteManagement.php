<?php

namespace Drupal\href_lang_exchange\Service;

use Drupal\href_lang_exchange\Connection\SiteManagementInterface;

/**
 * Class Site Management.
 *
 * @package Drupal\href_lang_exchange\Service
 */
class SiteManagement implements SiteManagementInterface {

  /**
   * {@inheritdoc}
   */
  public function getAllDestinationSites() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMasterSite() {
    return NULL;
  }

}
