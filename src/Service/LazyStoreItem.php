<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Provide NormalLazy load service.
 *
 * @package Drupal\href_lang_exchange\Service
 */
class LazyStoreItem implements LazyStoreItemJsonExtensionInterface, LazyStoreItemInterface {

  /**
   * This is the object for the method.
   *
   * @var object
   */
  protected $object;

  /**
   * This is the name of the method that should be invoked.
   *
   * @var string
   */
  protected $method;

  /**
   * This a the parameter/attributes to invoke the method.
   *
   * @var array
   */
  protected $attributes;

  /**
   * {@inheritdoc}
   */
  public function __construct($object, $method, array $attributes) {
    $this->object = $object;
    $this->method = $method;
    $this->attributes = $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getObject() {
    return $this->object;
  }

  /**
   * {@inheritdoc}
   */
  public function setObject($object) {
    $this->object = $object;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * {@inheritdoc}
   */
  public function setMethod($method) {
    $this->method = $method;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttributes() {
    return $this->attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function setAttributes(array $attributes) {
    $this->attributes = $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getInternalId() {
    if (isset($this->attributes[1]['data']['attributes']['internal_id'])) {
      return $this->attributes[1]['data']['attributes']['internal_id'];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode() {
    if (isset($this->attributes[1]['data']['attributes']['language'])) {
      return $this->attributes[1]['data']['attributes']['language'];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($path) {
    $this->attributes[1]['data']['attributes']['path'] = $path;
  }

}
