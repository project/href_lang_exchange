<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Interface Lazy Store Interface.
 *
 * @package Drupal\href_lang_exchange\Service
 */
interface LazyStoreInterface {

  /**
   * Add a object to Lazy Store.
   *
   * @param mixed $object
   *   The object on which the method should be called.
   * @param string $method
   *   The method that should be called.
   * @param mixed[] $attributes
   *   And the parameter for the method.
   */
  public function addToStore($object, $method, array $attributes);

  /**
   * Return the Lazy Store.
   *
   * @return \Drupal\href_lang_exchange\Service\LazyStoreItem[]
   *   A array with the addToStore structure.
   */
  public function getStore();

  /**
   * Add a lazy store item to a lazy store.
   *
   * @param \Drupal\href_lang_exchange\Service\LazyStoreItemInterface $item
   *   A lazy store item that should be added.
   */
  public function addItem(LazyStoreItemInterface $item);

}
