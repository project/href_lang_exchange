<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Provide Lazy load service.
 *
 * @package Drupal\href_lang_exchange\Service
 */
class LazyStore implements LazyStoreInterface {

  /**
   * The Lazy Store items.
   *
   * @var array
   */
  protected $store = [];

  /***
   * {@inheritDoc}
   */
  public function addToStore($object, $method, array $attributes) {

    $this->store[] = new LazyStoreItem($object, $method, $attributes);

  }

  /***
   * {@inheritDoc}
   */
  public function addItem(LazyStoreItemInterface $item) {
    $this->store[] = $item;
  }

  /***
   * {@inheritDoc}
   */
  public function getStore() {
    return $this->store;
  }

}
