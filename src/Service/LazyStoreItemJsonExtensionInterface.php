<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Interface Lazy Store Item Json Extension Interface.
 *
 * @package Drupal\href_lang_exchange\Service
 */
interface LazyStoreItemJsonExtensionInterface {

  /**
   * This method search in the attributes for a internal id.
   *
   * @return string|null
   *   Returm the internal id or null.
   */
  public function getInternalId();

  /**
   * This method search in the attributes for a Langcode.
   *
   * @return string|null
   *   Returm the internal id or null.
   */
  public function getLangcode();

  /**
   * Set the path attribute for the json api request.
   *
   * @param string $path
   *   The path string.
   */
  public function setPath($path);

}
