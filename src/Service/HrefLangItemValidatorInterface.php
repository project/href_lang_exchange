<?php

namespace Drupal\href_lang_exchange\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface Href Lang Item Validator Interface.
 *
 * @package Drupal\href_lang_exchange\Service
 */
interface HrefLangItemValidatorInterface {

  /**
   * This method validate the form.
   *
   * This method check if the entered gid-region-langcode are
   * unique in the distributed system.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the current validation should operate upon.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $element
   *   An associative array containing the structure of the form.
   */
  public function validateByUniqueTypes(EntityInterface $entity, FormStateInterface $form_state, array $element);

}
