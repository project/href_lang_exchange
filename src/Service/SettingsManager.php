<?php

namespace Drupal\href_lang_exchange\Service;

/**
 * Provide Settings Manager service.
 *
 * @package Drupal\href_lang_exchange\Service
 */
class SettingsManager implements SettingsManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function isAdminQueued() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isClientQueued() {
    return FALSE;
  }

}
