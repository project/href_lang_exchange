<?php

namespace Drupal\href_lang_exchange;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for HrefLangItem entities.
 */
class HrefLangItemStorage extends SqlContentEntityStorage implements HrefLangItemStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByInternalId($id) {
    $result = $this->getQuery()->condition('internal_id', $id)->execute();
    $result = $this->loadMultiple($result);
    if (!empty($result) && is_array($result)) {
      $result = array_shift($result);
      return $result;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllByInternalId($id) {
    $result = $this->getQuery()->condition('internal_id', $id)->execute();
    $result = $this->loadMultiple($result);
    if (!empty($result) && is_array($result)) {
      return $result;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllByGid($gid, $status = '') {
    $query = $this->getQuery()->condition('gid', $gid);

    if (!empty($status) && is_numeric($status)) {
      $query->condition('status', $status);
    }

    $result = $query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAllGidByRegionAndLanguage($region, $language) {
    $filter = [];

    $query_groups = $this->getAggregateQuery();

    $condition = $query_groups->andConditionGroup();

    $condition->condition('language', strtolower($region), '=');
    $condition->condition('region', strtoupper($language), '=');

    $query_groups->groupBy('gid')
      ->aggregate('id', 'count');

    $query_groups->condition($condition);
    $result_set = $query_groups->execute();

    foreach ($result_set as $item) {
      $filter[] = $item['gid'];
    }

    return $filter;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllUnconnectedItems() {
    $filter = [];
    $query_groups = $this->getAggregateQuery();

    $query_groups->groupBy('gid')
      ->conditionAggregate('id', 'count', 1);

    $result_set = $query_groups->execute();

    foreach ($result_set as $item) {
      $filter[] = $item['gid'];
    }

    return $filter;
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllByPath($path) {
    $result = $this->getQuery()->condition('path', $path)->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllLanguages() {
    $filter = [];

    $query_groups = $this->getAggregateQuery();

    $query_groups->groupBy('language')
      ->aggregate('id', 'count');

    $result_set = $query_groups->execute();

    foreach ($result_set as $item) {
      $filter[] = $item['language'];
    }

    return $filter;
  }

}
