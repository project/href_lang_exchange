<?php

namespace Drupal\href_lang_exchange;

/**
 * Contains all states for the HrefLangItem.
 */
final class HrefLangItemState {
  const NOT_PUBLISHED = 0;
  const PUBLISHED = 1;
  const NO_SYNCED = 3;

}
