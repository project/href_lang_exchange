CONTENTS OF THIS FILE
---------------------

* Introduction
* Permissions
* Requirements
* Recommended modules
* Installation
* Configuration
* Usage
* Notes
* Maintainers

INTRODUCTION
------------

The Module Collection has two Entity types.

From the href_lang_exchange_href module the ```site_entity``` which can be accessed via the
```/admin/structure/site_entity/settings``` can be extended.

The creation of these site entities is only done on the master.
The administration page can be found under ```/admin/structure/site_entity```.
Furthermore, there is a Settings Form (/admin/structure/site_entity/basic_settings) which is not available for the Site Editor.
There you can set the configuration for the authentication and the url for the master server.

The second entity type is the ```href_lang_item which``` is present in the base module ```href_lang_exchange```.
this is used to exchange the href lang information and is uninteresting for the end user. To
debug or administrate can the module ```href_lang_entity_manager``` be activated it provides a view which
gives an overview of all href lang items.

PERMISSIONS
-----------

### Permission List:
- add site entity entities:
- administer site entity entities:
- delete site entity entities:
- edit site entity entities:
- view published site entity entities:
- view unpublished site entity entities:
- view all site entity revisions:
- revert all site entity revisions:
- delete all site entity revisions:
- view href_lang_item entity:
- add href_lang_item entity:
- list site entity entities
- edit href_lang_item entity:
- delete href_lang_item entity:
- administer href_lang_item entity:
- administer href-lang exchange:

### Permission usage:
For the admin menu toolbar you need the following permissions.
- Use the administration toolbar
- Administer menus and menu items
- Use the administration pages and help

For the ```/href_lang_item/list and all href_lang_item``` things
- View the administration theme:
  - To see the theme
- Administer href-lang exchange:
  - Site Access
- view href_lang_item entity:
  - To view href_lang_items
- add href_lang_item entity:
  - To add href_lang_items
- edit href_lang_item entity:
  - To edit/update href_lang_items
- delete href_lang_item entity:
  - To delete href_lang_items

For the Management of Site entities (Administration Only)

Create new Site entities
- Delete Site entities
- Edit Site entities
- List Site entities
- View all Site revisions
- View published Site entities
- View unpublished Site entities

For real admin stuff you need this permission
- list site entity entities

REQUIREMENTS
------------

First of, all the modules on which it depends might be installed:

- Country - https://www.drupal.org/project/country
- Languagefield - https://www.drupal.org/project/languagefield

```
composer require 'drupal/languagefield:^1.8' composer require 'drupal/country:^1.0'
```

If you are NOT using composer, you would need to download the modules under ```web/modules/contrib```

RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

The following modules and its dependencies might be activated under ```/admin/modules```:

Install the modules like any other Drupal module.

### href_lang_exchange
- dependencies:
    - field
    - jsonapi

### href_lang_exchange_lang_country_override
- dependencies:
  - field
  - href_lang_exchange
  - country
  - languagefield

### href_lang_exchange_custom_logos
- dependencies:
  - field
  - href_lang_exchange
  - country
  - languagefield
  - href_lang_exchange_country_override
  - href_lang_exchange_href
  
### href_lang_exchange_entity_manager
- dependencies:
    - field
    - jsonapi
    - href_lang_exchange
    - views

### href_lang_exchange_href
- dependencies:
    - field
    - href_lang_exchange
    - country
    - languagefield

### href_lang_exchange_import_export
- dependencies:
  - href_lang_exchange
  - href_lang_exchange_entity_manager
  - href_lang_exchange_href

CONFIGURATION
-------------

Once every required module has been installed, you would need to follow the next steps in order to configure the module:

This configuration would simulate a scenario with three sites, Master site (UK site) and two slaves (DE site and US site). The configuration needs to be done on each environment.

1. Go to Configuration > Web services > JSON:API (/admin/config/services/jsonapi) and configure it to "Accept all JSON:API create, read, update, and delete operations".
2. Go to Configuration > Regional and language > Regional settings (/admin/config/regional/settings) and set the default country for each site.
3. Go to Structure > Site list > Settings (/admin/structure/site_entity/basic_settings) and configure the API endpoint url with the Master site url and an authentication key (it might be the same for each site), after that there are the options to queue the process. If these are not active, the system
   execute it directly which can cause performance problems with larger constructions.
4. Go to Structure > Site list (/admin/structure/site_entity) and add each site (UK, DE and US). The sites might be connected with each other.

USAGE (Example of simple scenario)
----------------------------------

Once the sites have been configured, everything is ready to start to use the functionality provided for the modules.

1. You would need to add field of type 'Href lang exchange item' to the entity where would you like to use it.
2. Once you create an entity using the 'href lang exchange item' field on any of the slave sites, this will be automatically reflected on the master site.
3. To check it, on the Master site go to  Structure > Href lang item List (/admin/structure/href_lang_item/list).
4. Finally, to have the option to switch between the different entities which are using the functionality, go to Structure > Block layout (/admin/structure/block) and place the href lang item language switcher to the section where would you like to show it.

NOTES
-----

IMPORTANT: If you are on a monolingual site, please use the "default_href_lang_exchange".

The setting is under ```admin/config/search/simplesitemap/variants``` 

The following might be entered in the text field:
```
  default | default_href_lang_exchange | default
```

MAINTAINERS
-----------

Current maintainers:
- David Galeano (gxleano) - https://www.drupal.org/u/gxleano
- Alvaro Hurtado (alvar0hurtad0) - https://www.drupal.org/u/alvar0hurtad0
- Alexander Dross (alexanderdross) - https://www.drupal.org/u/alexanderdross
- Shibin Das (D34dMan) - https://www.drupal.org/u/d34dman
- Volkan Jacobsen (muschpusch) - https://www.drupal.org/u/muschpusch
- Stephan Huber (stmh) - https://www.drupal.org/u/stmh
