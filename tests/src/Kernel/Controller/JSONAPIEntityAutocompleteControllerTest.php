<?php

namespace Drupal\href_lang_exchange\Controller;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Controller\ControllerBase;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;
use Drupal\href_lang_exchange\HrefLangItemStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class JSONAPIEntityAutocompleteControllerTest extends ControllerBase {

  /**
   * The JSON:API serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\Symfony\Component\Serializer\Normalizer\DenormalizerInterface
   */
  protected $serializer;

  /**
   * A Connection instance.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $connection;

  /**
   * The hreflangitem storage engine.
   *
   * @var \Drupal\href_lang_exchange\HrefLangItemStorageInterface
   */
  protected $storage;

  /**
   * Constructs a EntityAutocompleteController object.
   *
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The JSON:API serializer.
   * @param \\Drupal\href_lang_exchange\Connection\ConnectionInterface $connection
   *   A Connection instance.
   * @param \Drupal\href_lang_exchange\HrefLangItemStorageInterface $storage
   *   The storage for the hreflangitems.
   */
  public function __construct(SerializerInterface $serializer, ConnectionInterface $connection, HrefLangItemStorageInterface $storage) {

    $this->serializer = $serializer;
    $this->connection = $connection;
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('jsonapi.serializer'),
      $container->get('href_lang_exchange.connection'),
      $container->get('entity_type.manager')->getStorage('href_lang_item')
    );
  }

  /**
   * Autocomplete the label of an entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object that contains the typed tags.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The matched entity labels as a JSON response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown if the selection settings key is not found in the key/value store
   *   or if it does not match the stored data.
   */
  public function handleAutocomplete(Request $request) {
    $matches = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $elements = [];

      $typed_string = Tags::explode($input);
      $typed_string = mb_strtolower(array_pop($typed_string));

      $query = $this->storage->getQuery();

      if ($language = $request->query->get('lang')) {
        $typed_language_string = Tags::explode($language);
        $typed_language_string = mb_strtolower(array_pop($typed_language_string));
        $query->condition('language', $typed_language_string);
      }

      $condition = $query->orConditionGroup();
      $condition->condition('title', $typed_string, 'CONTAINS');
      $condition->condition('gid', $typed_string, 'CONTAINS');
      $query->condition($condition)->pager(20);

      \Drupal::moduleHandler()->alter('href_lang_autocomplete', $query, $request);

      $result = $query->execute();

      /** @var \Drupal\href_lang_exchange\Entity\HrefLangItem[] $resultes */
      $resultes = $this->storage->loadMultiple($result);
      $languages = \Drupal::languageManager()->getStandardLanguageList();
      foreach ($resultes as $entity) {

        $value['gid'] = $entity->getGid();
        $value['title'] = $entity->getTitle();
        $value['region'] = $entity->getRegion();
        $value['language'] = $entity->getLanguage();
        $value['language_label'] = $entity->getLanguage();
        $value['url'] = $entity->getPath();

        if (isset($languages[$entity->getLanguage()])) {
          $value['language_label'] = $languages[$entity->getLanguage()][0];
        }
        $value['elements'] = [];

        $entities = $this->storage->loadAllByGid($entity->getGid());

        foreach ($entities as $element) {

          if (
            $entity->id() != $element->id() &&
            $entity->getPath() != $element->getPath()
          ) {
            $value['elements'][] = [
              'title' => $element->getTitle(),
              'url' => $element->getPath(),
              'region' => $element->getRegion(),
              'language' => $element->getLanguage(),
            ];
          }
        }
        $matches[] = $value;
      }
    }

    \Drupal::moduleHandler()
      ->alter('jsonapi_autocomplete_filter_results', $matches);

    // @todo maybe authorization.
    return new JsonResponse(array_values($matches), 200, ['Access-Control-Allow-Origin' => '*']);
  }

  /**
   * Return all languages that currently exist as href lang item.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The Response.
   */
  public function handleSearchFacet() {

    $languages = \Drupal::languageManager()->getStandardLanguageList();
    $all_used_languages = [];

    $languages_el = $this->storage->loadAllLanguages();

    foreach ($languages_el as $item) {
      $all_used_languages[$item] = $languages[$item][0];
    }

    return new JsonResponse($all_used_languages, 200, ['Access-Control-Allow-Origin' => '*']);
  }

}
