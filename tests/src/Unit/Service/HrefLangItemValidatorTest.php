<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Service;

use Drupal\href_lang_exchange\Service\HrefLangItemValidator;
use Drupal\jsonapi\Query\EntityConditionGroup;
use Drupal\Tests\PhpunitCompatibilityTrait;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Service\HrefLangItemValidator
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangItemValidatorTest extends UnitTestCase {

  use PhpunitCompatibilityTrait;

  /**
   * @covers ::__construct
   * @dataProvider constructProvider
   */
  public function testConstruct($case) {

    $client = $this->getMockBuilder('GuzzleHttp\ClientInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $queue = $this->getMockBuilder('Drupal\Core\Queue\QueueFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();

    // $connection = new Connection($client, $queue, $logger);
    $this->assertEquals(TRUE, TRUE);
  }

  /**
   * Test values for the __construct method.
   *
   * @return array
   *   The provider array.
   */
  public function constructProvider() {
    return [
      [['value' => 1]],
      [['value' => 'z']],
      [['value' => '42']],
      [['value' => TRUE]],
      [['value' => FALSE]],
    ];
  }

  /**
   * @covers ::__construct
   */
  public function testConstructException() {
    $this->setExpectedException(\InvalidArgumentException::class);
    new EntityConditionGroup('NOT_ALLOWED', []);
  }

  /**
   * @covers ::validateByUniquePath
   * @dataProvider validateByUniquePathProvider
   */
  public function testValidateByUniquePath($case) {

    $connection = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\ConnectionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $url = $this->getMockBuilder('\Drupal\Core\Url')
      ->disableOriginalConstructor()
      ->getMock();
    $entity = $this->getMockBuilder('Drupal\Core\Entity\EntityInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $entity->expects($this->any())
      ->method('toUrl')
      ->will($this->returnValue($url));

    $form_state = $this->getMockBuilder('Drupal\Core\Form\FormStateInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $configa = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $configa->expects($this->any())
      ->method('get')->with('country.default')
      ->will($this->returnValue('AF'));

    $config = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $config->expects($this->any())
      ->method('get')->with('system.date')
      ->will($this->returnValue($configa));

    $site_interface = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $translation = $this->getMockBuilder('Drupal\Core\StringTranslation\TranslationInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $form = [];
    $mock = $this->createMock('Symfony\Component\DependencyInjection\ContainerBuilder');

    $mock->expects($this->any())
      ->method('get')
      ->withConsecutive(['config.factory'], ['string_translation'])
      ->willReturnOnConsecutiveCalls($this->returnValue($config), $this->returnValue($translation));
    \Drupal::setContainer($mock);

    $connection->expects($this->once())
      ->method('getMaster')
      ->will($this->returnValue($site_interface));

    $href_lang_item_validator = new HrefLangItemValidator($connection, $logger);

    $href_lang_item_validator->validateByUniquePath($entity, $form_state, $form);

  }

  /**
   * Test values for the validateByUniquePath method.
   *
   * @return array
   *   The provider array.
   */
  public function validateByUniquePathProvider() {
    return [
      [['value' => 1]],
      [['value' => 'z']],
      [['value' => '42']],
      [['value' => TRUE]],
      [['value' => FALSE]],
      [
        [
          'value' => $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteInterface')
            ->disableOriginalConstructor()
            ->getMock(),
        ],
      ],
    ];
  }

  /**
   * @covers ::validateByUniqueTypes
   * @dataProvider validateByUniqueTypesProvider
   */
  public function testValidateByUniqueTypes($case) {

    $connection = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\ConnectionInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $entity = $this->getMockBuilder('Drupal\Core\Entity\EntityInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $form_state = $this->getMockBuilder('Drupal\Core\Form\FormStateInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $configa = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $configa->expects($this->any())
      ->method('get')->with('country.default')
      ->will($this->returnValue('AF'));

    $config = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $config->expects($this->any())
      ->method('get')->with('system.date')
      ->will($this->returnValue($configa));

    $site_interface = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $translation = $this->getMockBuilder('Drupal\Core\StringTranslation\TranslationInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $form = [];
    $mock = $this->createMock('Symfony\Component\DependencyInjection\ContainerBuilder');

    $mock->expects($this->any())
      ->method('get')
      ->withConsecutive(['config.factory'], ['string_translation'])
      ->willReturnOnConsecutiveCalls($this->returnValue($config), $this->returnValue($translation));
    \Drupal::setContainer($mock);

    $connection->expects($this->once())
      ->method('getMaster')
      ->will($this->returnValue($site_interface));

    $href_lang_item_validator = new HrefLangItemValidator($connection, $logger);

    $href_lang_item_validator->validateByUniqueTypes($entity, $form_state, $form);

  }

  /**
   * Test values for the getMaster method.
   *
   * @return array
   *   The provider array.
   */
  public function validateByUniqueTypesProvider() {
    return [
      [['value' => 1]],
      [['value' => 'z']],
      [['value' => '42']],
      [['value' => TRUE]],
      [['value' => FALSE]],
      [
        [
          'value' => $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteInterface')
            ->disableOriginalConstructor()
            ->getMock(),
        ],
      ],
    ];
  }

}
