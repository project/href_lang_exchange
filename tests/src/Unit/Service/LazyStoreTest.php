<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Service;

use Drupal\href_lang_exchange\Service\LazyStore;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Service\LazyStore
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class LazyStoreTest extends UnitTestCase {

  /**
   * @covers ::addToStore
   */
  public function testAddToStore() {
    $lazy_store = new LazyStore();
    $lazy_store->addToStore();
  }

  /**
   * @covers ::addItem
   */
  public function testAddItem() {
    $lazy_store = new LazyStore();
    $lazy_store->addItem();
  }

  /**
   * @covers ::getStore
   */
  public function testGetStore() {
    $lazy_store = new LazyStore();
    $lazy_store->getStore();
  }

}
