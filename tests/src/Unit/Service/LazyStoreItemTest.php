<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Service;

use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Service\LazyStoreItem
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class LazyStoreItemTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testConstruct() {

  }

  /**
   * @covers ::getObject
   */
  public function testGetObject() {
    return $this->object;
  }

  /**
   * @covers ::setObject
   */
  public function testSetObject($object) {
    $this->object = $object;
  }

  /**
   * @covers ::getMethod
   */
  public function testGetMethod() {
    return $this->method;
  }

  /**
   * @covers ::setMethod
   */
  public function testSetMethod($method) {
    $this->method = $method;
  }

  /**
   * @covers ::getAttributes
   */
  public function testGetAttributes() {
    return $this->attributes;
  }

  /**
   * @covers ::setAttributes
   */
  public function testSetAttributes(array $attributes) {
    $this->attributes = $attributes;
  }

  /**
   * @covers ::getInternalId
   */
  public function testGetInternalId() {
    if (isset($this->attributes[1]['data']['attributes']['internal_id'])) {
      return $this->attributes[1]['data']['attributes']['internal_id'];
    }
    return NULL;
  }

  /**
   * @covers ::getLangcode
   */
  public function testGetLangcode() {
    if (isset($this->attributes[1]['data']['attributes']['language'])) {
      return $this->attributes[1]['data']['attributes']['language'];
    }
    return NULL;
  }

  /**
   * @covers ::setPath
   */
  public function testSetPath($path) {
    $this->attributes[1]['data']['attributes']['path'] = $path;
  }

}
