<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Service;

use Drupal\href_lang_exchange\Service\NormalLazyStore;
use Drupal\jsonapi\Query\EntityConditionGroup;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Service\NormalLazyStore
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class NormalLazyStoreTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testConstrutur() {
    $test = new NormalLazyStore(new \stdClass(), 'test', []);
    $this->assertTrue(TRUE);
  }

  /**
   * @covers ::__construct
   */
  public function testConstructException() {
    $this->setExpectedException(\InvalidArgumentException::class);
    new EntityConditionGroup('NOT_ALLOWED', []);
  }

  /**
   * @covers ::getObject
   */
  public function testGetObject() {
    $test = new NormalLazyStore(new \stdClass(), 'test', []);
    $test->getObject();
  }

  /**
   * @covers ::setObject
   */
  public function testSetObject() {
    $test = new NormalLazyStore(new \stdClass(), 'test', []);
  }

  /**
   * @covers ::getMethod
   */
  public function testgetMethod() {
    return $this->method;
  }

  /**
   * @covers ::setMethod
   */
  public function testSetMethod($method) {
    $this->method = $method;
  }

  /**
   * @covers ::getAttributes
   */
  public function testGetAttributes() {
    return $this->attributes;
  }

  /**
   * @covers ::setAttributes
   */
  public function testSetAttributes(array $attributes) {
  }

}
