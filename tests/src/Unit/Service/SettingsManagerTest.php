<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Service;

use Drupal\href_lang_exchange\Service\SettingsManager;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Service\SettingsManager
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class SettingsManagerTest extends UnitTestCase {

  /**
   * @covers ::isClientQueued
   */
  public function testIsClientQueued() {
    $settings_manager = new SettingsManager();
    $this->assertEquals(FALSE, $settings_manager->isClientQueued());
    $this->assertEmpty($settings_manager->isClientQueued());
  }

  /**
   * @covers ::isAdminQueued
   */
  public function testIsAdminQueued() {
    $settings_manager = new SettingsManager();
    $this->assertEquals(FALSE, $settings_manager->isAdminQueued());
    $this->assertEmpty($settings_manager->isAdminQueued());
  }

}
