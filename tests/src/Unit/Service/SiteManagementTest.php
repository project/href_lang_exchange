<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Service;

use Drupal\href_lang_exchange\Service\SiteManagement;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Service\SiteManagement
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class SiteManagementTest extends UnitTestCase {

  /**
   * @covers ::getAllDestinationSites
   */
  public function testGetAllDestinationSites() {
    $site_management = new SiteManagement();
    $this->assertEquals([], $site_management->getAllDestinationSites());
  }

  /**
   * @covers ::getMasterSite
   */
  public function testGetMasterSite() {
    $site_management = new SiteManagement();
    $this->assertEquals(NULL, $site_management->getMasterSite());
    $this->assertEmpty($site_management->getMasterSite());
  }

}
