<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Controller;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;
use Drupal\href_lang_exchange\Controller\JSONAPIEntityAutocompleteController;
use Drupal\href_lang_exchange\HrefLangItemStorageInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Controller\JSONAPIEntityAutocompleteController
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class JSONAPIEntityAutocompleteControllerTest extends UnitTestCase {

  /**
   * @covers ::handleAutocomplete
   * @dataProvider handleAutocompleteProvider
   */
  public function testHandleAutocomplete($case) {
    $send_validator = new JSONAPIEntityAutocompleteController($case['serializer'], $case['connection'], $case['storage'], $case['module_handler']);
    $response = $send_validator->handleAutocomplete($case['request']);
    $this->assertEquals($case['expect']['status'], $response->getStatusCode());
    $this->assertEquals($case['expect']['headers']['access-control-allow-origin'], $response->headers->get('access-control-allow-origin'));
    $this->assertEquals($case['expect']['headers']['cache-control'], $response->headers->get('cache-control'));
    $this->assertEquals($case['expect']['headers']['content-type'], $response->headers->get('content-type'));
    $this->assertEquals($case['expect']['content'], $response->getContent());
  }

  /**
   * Test values for the handleAutocomplete method.
   *
   * @return array
   *   The provider array.
   */
  public function handleAutocompleteProvider() {
    $default_serializer = $this->getMockBuilder(SerializerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_module_handler = $this->getMockBuilder(ModuleHandler::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_module_handler->expects($this->any())
      ->method('alter')
      ->will($this->returnValue([]));

    $default_connection = $this->getMockBuilder(ConnectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection->expects($this->any())
      ->method('checkSelfMaster')
      ->will($this->returnValue(TRUE));

    $default_storage = $this->getMockBuilder(HrefLangItemStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_storage->expects($this->any())
      ->method('loadMultiple')
      ->will($this->returnValue([]));

    $default_storage->expects($this->any())
      ->method('loadAllByGid')
      ->will($this->returnValue([]));

    $default_query = $this->getMockBuilder(QueryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_query->expects($this->any())
      ->method('condition')
      ->will($this->returnValue($default_query));

    $default_query->expects($this->any())
      ->method('orConditionGroup')
      ->will($this->returnValue($default_query));

    $default_query->expects($this->any())
      ->method('pager')
      ->will($this->returnValue($default_query));
    $default_query->expects($this->any())
      ->method('execute')
      ->will($this->returnValue([]));

    $default_storage->expects($this->any())
      ->method('getQuery')
      ->will($this->returnValue($default_query));

    $default_elements = [
      [
        [
          'serializer' => $default_serializer,
          'connection' => $default_connection,
          'module_handler' => $default_module_handler,
          'storage' => $default_storage,
          'request' => new Request(['q' => 'hello world', 'lang' => 'test']),
          'expect' => [
            'status' => 200,
            'headers' => [
              'access-control-allow-origin' => '*',
              'cache-control' => 'no-cache, private',
              'content-type' => 'application/json',
            ],
            'content' => '[]',
          ],
        ],
      ],
    ];

    return $default_elements;
  }

  /**
   * {@inheritdoc}
   */
  public function handleSearchFacet() {

    $languages = \Drupal::languageManager()->getStandardLanguageList();
    $all_used_languages = [];

    $languages_el = $this->storage->loadAllLanguages();

    foreach ($languages_el as $item) {
      $all_used_languages[$item] = $languages[$item][0];
    }

    return new JsonResponse($all_used_languages, 200, ['Access-Control-Allow-Origin' => '*']);
  }

  /**
   * @covers ::handleSearchFacet
   * @dataProvider handleSearchFacetProvider
   */
  public function testHandleSearchFacet($case) {
    $send_validator = new JSONAPIEntityAutocompleteController($case['serializer'], $case['connection'], $case['storage'], $case['module_handler']);
    $response = $send_validator->handleSearchFacet($case['request']);
    $this->assertEquals($case['expect']['status'], $response->getStatusCode());
    $this->assertEquals($case['expect']['headers']['access-control-allow-origin'], $response->headers->get('access-control-allow-origin'));
    $this->assertEquals($case['expect']['headers']['cache-control'], $response->headers->get('cache-control'));
    $this->assertEquals($case['expect']['headers']['content-type'], $response->headers->get('content-type'));
    $this->assertEquals($case['expect']['content'], $response->getContent());
  }

  /**
   * Test values for the handleSearchFacet method.
   *
   * @return array
   *   The provider array.
   */
  public function handleSearchFacetProvider() {
    $default_serializer = $this->getMockBuilder(SerializerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_module_handler = $this->getMockBuilder(ModuleHandler::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_module_handler->expects($this->any())
      ->method('alter')
      ->will($this->returnValue([]));

    $default_connection = $this->getMockBuilder(ConnectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection->expects($this->any())
      ->method('checkSelfMaster')
      ->will($this->returnValue(TRUE));

    $default_query = $this->getMockBuilder(QueryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_query->expects($this->any())
      ->method('condition')
      ->will($this->returnValue($default_query));

    $default_query->expects($this->any())
      ->method('orConditionGroup')
      ->will($this->returnValue($default_query));

    $default_query->expects($this->any())
      ->method('pager')
      ->will($this->returnValue($default_query));
    $default_query->expects($this->any())
      ->method('execute')
      ->will($this->returnValue([]));

    $default_storage = $this->createStorage([], $default_query);

    $default_elements = [
      [
        [
          'serializer' => $default_serializer,
          'connection' => $default_connection,
          'module_handler' => $default_module_handler,
          'storage' => $default_storage,
          'request' => new Request(['q' => 'hello world', 'lang' => 'test']),
          'expect' => [
            'status' => 200,
            'headers' => [
              'access-control-allow-origin' => '*',
              'cache-control' => 'no-cache, private',
              'content-type' => 'application/json',
            ],
            'content' => '[]',
          ],
        ],

        [
          'serializer' => $default_serializer,
          'connection' => $default_connection,
          'module_handler' => $default_module_handler,
          'storage' => $this->createStorage(['en'], $default_query),
          'request' => new Request(['q' => 'hello world', 'lang' => 'test']),
          'expect' => [
            'status' => 200,
            'headers' => [
              'access-control-allow-origin' => '*',
              'cache-control' => 'no-cache, private',
              'content-type' => 'application/json',
            ],
            'content' => '[]',
          ],
        ],

        [
          'serializer' => $default_serializer,
          'connection' => $default_connection,
          'module_handler' => $default_module_handler,
          'storage' => $this->createStorage(['something'], $default_query),
          'request' => new Request(['q' => 'hello world', 'lang' => 'test']),
          'expect' => [
            'status' => 200,
            'headers' => [
              'access-control-allow-origin' => '*',
              'cache-control' => 'no-cache, private',
              'content-type' => 'application/json',
            ],
            'content' => '[]',
          ],
        ],

        [
          'serializer' => $default_serializer,
          'connection' => $default_connection,
          'module_handler' => $default_module_handler,
          'storage' => $this->createStorage('', $default_query),
          'request' => new Request(['q' => 'hello world', 'lang' => 'test']),
          'expect' => [
            'status' => 200,
            'headers' => [
              'access-control-allow-origin' => '*',
              'cache-control' => 'no-cache, private',
              'content-type' => 'application/json',
            ],
            'content' => '[]',
          ],
        ],

        [
          'serializer' => $default_serializer,
          'connection' => $default_connection,
          'module_handler' => $default_module_handler,
          'storage' => $this->createStorage(new \stdClass(), $default_query),
          'request' => new Request(['q' => 'hello world', 'lang' => 'test']),
          'expect' => [
            'status' => 200,
            'headers' => [
              'access-control-allow-origin' => '*',
              'cache-control' => 'no-cache, private',
              'content-type' => 'application/json',
            ],
            'content' => '[]',
          ],
        ],
      ],
    ];

    return $default_elements;
  }

  /**
   * {@inheritdoc}
   */
  public function createStorage($loadAllLanguages, $getQuery = [], $loadMultiple = [], $loadAllByGid = []) {
    $default_storage = $this->getMockBuilder(HrefLangItemStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_storage->expects($this->any())
      ->method('loadMultiple')
      ->will($this->returnValue($loadMultiple));

    $default_storage->expects($this->any())
      ->method('loadAllByGid')
      ->will($this->returnValue($loadAllByGid));

    $default_storage->expects($this->any())
      ->method('loadAllLanguages')
      ->will($this->returnValue($loadAllLanguages));

    $default_storage->expects($this->any())
      ->method('getQuery')
      ->will($this->returnValue($getQuery));

    return $default_storage;
  }

}
