<?php

namespace Drupal\Tests\href_lang_exchange\Unit;

use Drupal\href_lang_exchange\Authorization\NoAuthorization;
use Drupal\href_lang_exchange\Connection\AuthorizationInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\HrefLangItemAccessControlHandler
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangItemAccessControlHandlerTest extends UnitTestCase {

  /**
   * @covers ::getMaster
   */
  public function testBasics() {
    $authorization = new NoAuthorization();
    $this->assertInstanceOf(AuthorizationInterface::class, $authorization);
  }

  /**
   * @covers ::getAuthHeader
   */
  public function testGetAuthHeader() {
    $authorization = new NoAuthorization();

    // Repeat it to be sure, that it is every time the same.
    $this->assertEmpty($authorization->getAuthHeader());
    $this->assertEquals($authorization->getAuthHeader(), '');

    $this->assertEmpty($authorization->getAuthHeader());
    $this->assertEquals($authorization->getAuthHeader(), '');

    $this->assertEmpty($authorization->getAuthHeader());
    $this->assertEquals($authorization->getAuthHeader(), '');
  }

  /**
   * @covers ::sendAuthHeader
   */
  public function testSendAuthHeader() {
    $authorization = new NoAuthorization();

    // Repeat it to be sure, that it is every time the same.
    $this->assertEmpty($authorization->sendAuthHeader());
    $this->assertEquals($authorization->sendAuthHeader(), FALSE);

    $this->assertEmpty($authorization->sendAuthHeader());
    $this->assertEquals($authorization->sendAuthHeader(), FALSE);

    $this->assertEmpty($authorization->sendAuthHeader());
    $this->assertEquals($authorization->sendAuthHeader(), FALSE);
  }

}
