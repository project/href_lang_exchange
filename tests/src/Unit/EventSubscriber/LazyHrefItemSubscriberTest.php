<?php

namespace Drupal\Tests\href_lang_exchange\Unit\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\href_lang_exchange\Connection\Connection;
use Drupal\href_lang_exchange\EventSubscriber\LazyHrefItemSubscriber;
use Drupal\href_lang_exchange\Service\LazyStoreInterface;
use Drupal\href_lang_exchange\Service\LazyStoreItem;
use Drupal\Tests\UnitTestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\EventSubscriber\LazyHrefItemSubscriber
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class LazyHrefItemSubscriberTest extends UnitTestCase {

  /**
   * @covers ::getSubscribedEvents
   */
  public function lazyHrefTerminate($case) {
    $value = LazyHrefItemSubscriber::getSubscribedEvents();
    $this->assertArrayEquals([
      KernelEvents::TERMINATE => 'lazyHrefTerminate',
    ], $value);

    $this->assertArrayEquals([
      KernelEvents::TERMINATE => 'lazyHrefTerminate',
    ], LazyHrefItemSubscriber::getSubscribedEvents());

    $this->assertArrayEquals([
      KernelEvents::TERMINATE => 'lazyHrefTerminate',
    ], LazyHrefItemSubscriber::getSubscribedEvents());
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {
    $lazy_store = $this->createLazyStore('');
    $subscriber = new LazyHrefItemSubscriber($lazy_store, $this->createEntityTypeManagerInterface(''), $this->createLoggerInterface(''));
    $this->assertEquals(TRUE, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function createEntityTypeManagerInterface($value) {
    $lazy_store = $this->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    return $lazy_store;
  }

  /**
   * {@inheritdoc}
   */
  public function createLoggerInterface($value) {
    $lazy_store = $this->getMockBuilder(LoggerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    return $lazy_store;
  }

  /**
   * @covers ::lazyHrefTerminate
   * @dataProvider lazyHrefTerminateProvider
   */
  public function testLazyHrefTerminate($case) {
    $subscriber = new LazyHrefItemSubscriber($case['lazy_store'], $case['entity_type_manager'], $case['logger']);
    $subscriber->lazyHrefTerminate();
    $this->assertTrue(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function lazyHrefTerminateProvider() {
    $lazy_element = $this->getMockBuilder(Connection::class)
      ->disableOriginalConstructor()
      ->getMock();

    $lazy_element->expects($this->exactly(4))
      ->method('getHrefLangItem')
      ->will($this->returnValue(42));

    return [
      [
        [
          'lazy_store' => $this->createLazyStore([]),
          'entity_type_manager' => $this->createEntityTypeManagerInterface([]),
          'logger' => $this->createLoggerInterface([]),
          'expect' => '',
        ],
        [
          'lazy_store' => $this->createLazyStore([
            'dsds',
            34,
            new \stdClass(),
            [],
            FALSE,
            TRUE,
          ]),
          'entity_type_manager' => $this->createEntityTypeManagerInterface([]),
          'logger' => $this->createLoggerInterface([]),
          'expect' => '',
        ],
        [
          'lazy_store' => $this->createLazyStore([
            new LazyStoreItem($lazy_element, 'getHrefLangItem', [
              ['data' => 'nice', 'attributes' => 'nicer', 'internal_id' => ''],
            ]),
          ]),
          'entity_type_manager' => $this->createEntityTypeManagerInterface([]),
          'logger' => $this->createLoggerInterface([]),
          'expect' => '',
        ],
        [
          'lazy_store' => $this->createLazyStore([
            new LazyStoreItem($lazy_element, 'getHrefLangItem', [
              ['data' => 'nice', 'attributes' => 'nicer', 'internal_id' => ''],
            ]),
          ]),
          'entity_type_manager' => $this->createEntityTypeManagerInterface([]),
          'logger' => $this->createLoggerInterface([]),
          'expect' => '',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createLazyStore($value) {
    $lazy_store = $this->getMockBuilder(LazyStoreInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $lazy_store->expects($this->any())
      ->method('getStore')
      ->will($this->returnValue($value));

    return $lazy_store;
  }

}
