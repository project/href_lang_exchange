<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Form;

use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Form\HrefLangItemForm
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangItemFormTest extends UnitTestCase {


  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The validation service.
   *
   * @var \Drupal\href_lang_exchange\Service\HrefLangItemValidator
   */
  protected $validator;

  /**
   * HrefLangItemForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\href_lang_exchange\Service\HrefLangItemValidatorInterface $validator
   *   The validator service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, HrefLangItemValidatorInterface $validator, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, MessengerInterface $messenger = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
    $this->validator = $validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('href_lang_exchange.service.href_lang_item_validator'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\href_lang_exchange\Entity\HrefLangItem $entity */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);
    // @todo check validation
    // $this->validator->validateByUniqueTypes($entity, $form_state, $form);.
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      $this->messenger->addMessage($this->t('The HrefLangItem %feed has been updated.', [
        '%feed' => $entity->toLink()
          ->toString(),
      ]));
    }
    else {
      $this->messenger->addMessage($this->t('The HrefLangItem %feed has been added.', [
        '%feed' => $entity->toLink()
          ->toString(),
      ]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));

    return $status;
  }

}
