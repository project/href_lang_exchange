<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Twig;

use Drupal\href_lang_exchange\Twig\TwigExtension;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Twig\TwigExtension
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class TwigExtensionTest extends UnitTestCase {

  /**
   * @covers ::getFilters
   */
  public function testGetFilters() {
    $extension = new TwigExtension();
    $extension->getFilters();

    $this->assertEquals([
      new \Twig_SimpleFilter('t_langcode', [$extension, 'getLangcodeLabel']),
      new \Twig_SimpleFilter('t_region', [$extension, 'getRegionLabel']),
    ], $extension->getFilters());
  }

  /**
   * @covers ::getRegionLabel
   * @dataProvider getRegionLabelProvider
   */
  public function testGetRegionLabel($case) {
    $extension = new TwigExtension();
    $this->assertEquals($case['expect'], $extension->getRegionLabel($case['arg']));
  }

  /**
   * Test values for the getRegionLabel method.
   *
   * @return array
   *   The provider array.
   */
  public function getRegionLabelProvider() {

    $default_elements = [
      [
        [
          'arg' => '',
          'expect' => NULL,
        ],
        [
          'arg' => new \stdClass(),
          'expect' => NULL,
        ],
        [
          'arg' => 3,
          'expect' => NULL,
        ],
        [
          'arg' => [],
          'expect' => NULL,
        ],

        [
          'arg' => ['a'],
          'expect' => NULL,
        ],
        [
          'arg' => TRUE,
          'expect' => NULL,
        ],
        [
          'arg' => FALSE,
          'expect' => NULL,
        ],
        [
          'arg' => 'en',
          'expect' => ['a'],
        ],
      ],
    ];

    return $default_elements;
  }

  /**
   * @covers ::getRegionLabel
   * @dataProvider getLangcodeLabelProvider
   */
  public function testGetLangcodeLabel($case) {
    $extension = new TwigExtension();
    $this->assertEquals($case['expect'], $extension->getLangcodeLabel($case['arg']));
  }

  /**
   * Test values for the getRegionLabel method.
   *
   * @return array
   *   The provider array.
   */
  public function getLangcodeLabelProvider() {

    $default_elements = [
      [
        [
          'arg' => '',
          'expect' => NULL,
        ],
        [
          'arg' => new \stdClass(),
          'expect' => NULL,
        ],
        [
          'arg' => 3,
          'expect' => NULL,
        ],
        [
          'arg' => [],
          'expect' => NULL,
        ],

        [
          'arg' => ['a'],
          'expect' => NULL,
        ],
        [
          'arg' => TRUE,
          'expect' => NULL,
        ],
        [
          'arg' => FALSE,
          'expect' => NULL,
        ],
        [
          'arg' => 'en',
          'expect' => ['a'],
        ],
      ],
    ];

    return $default_elements;
  }

}
