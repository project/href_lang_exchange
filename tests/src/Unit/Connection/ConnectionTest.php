<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Connection;

use Drupal\href_lang_exchange\Connection\Connection;
use Drupal\href_lang_exchange\Connection\SiteInterface;
use Drupal\jsonapi\Query\EntityConditionGroup;
use Drupal\Tests\PhpunitCompatibilityTrait;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Connection\Connection
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class ConnectionTest extends UnitTestCase {

  use PhpunitCompatibilityTrait;


  /**
   * The mock container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $container;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerBuilder')
      ->setMethods(['get'])
      ->getMock();
  }

  /**
   * @covers ::__construct
   * @dataProvider constructProvider
   */
  public function testConstruct($case) {

    $client = $this->getMockBuilder('GuzzleHttp\ClientInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $queue = $this->getMockBuilder('Drupal\Core\Queue\QueueFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $authorization = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\AuthorizationInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $siteManagement = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteManagementInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $connection = new Connection($client, $queue, $logger, $authorization, $siteManagement);
    $group = new EntityConditionGroup($case['conjunction'], $case['members']);

    $this->assertEquals(TRUE, TRUE);

  }

  /**
   * @covers ::__construct
   */
  public function testConstructException() {
    $this->setExpectedException(\InvalidArgumentException::class);
    new EntityConditionGroup('NOT_ALLOWED', []);
  }

  /**
   * @covers ::getMaster
   * @dataProvider getMasterProvider
   */
  public function testGetMaster($case) {

    if (!($case['value'] instanceof SiteInterface)) {
      $this->setExpectedException(\Exception::class, 'No master is defined.');
    }

    $client = $this->getMockBuilder('GuzzleHttp\ClientInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $queue = $this->getMockBuilder('Drupal\Core\Queue\QueueFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $authorization = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\AuthorizationInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $siteManagement = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteManagementInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $storage = $this->getMockBuilder('Drupal\href_lang_exchange_href\SiteEntityStorageInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $storage->expects($this->once())
      ->method('getMaster')
      ->will($this->returnValue($case['value']));

    $typeManager = $this->getMockBuilder('Drupal\Core\Entity\EntityTypeManagerInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $typeManager->expects($this->once())
      ->method('getStorage')
      ->with('site_entity')
      ->will($this->returnValue($storage));

    $this->setMockContainerService('entity_type.manager', $typeManager);

    $connection = new Connection($client, $queue, $logger, $authorization, $siteManagement);

    $master = $connection->getMaster();

    if ($case['value'] instanceof SiteInterface) {
      $this->assertInstanceOf('Drupal\href_lang_exchange\Connection\SiteInterface', $case['value']);
    }
  }

  /**
   * Test values for the getMaster method.
   *
   * @return array
   *   The provider array.
   */
  public function getMasterProvider() {
    return [
      [['value' => 1]],
      [['value' => 'z']],
      [['value' => '42']],
      [['value' => TRUE]],
      [['value' => FALSE]],
      [
        [
          'value' => $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteInterface')
            ->disableOriginalConstructor()
            ->getMock(),
        ],
      ],
    ];
  }

  /**
   * @covers ::deleteHrefLangItem
   * @dataProvider constructProvider
   */
  public function deleteHrefLangItem($case) {
  }

  /**
   * @covers ::updateHrefLangItem
   * @dataProvider constructProvider
   */
  public function updateHrefLangItem($case) {
  }

  /**
   * @covers ::getHrefLangItem
   * @dataProvider getHrefLangItemProvider
   */
  public function testGetHrefLangItem($case) {

    $url = $case['url'];
    $filter = $case['filter'];
    $response = $case['response'];

    $queue = $this->getMockBuilder('Drupal\Core\Queue\QueueFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $authorization = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\AuthorizationInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $siteManagement = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteManagementInterface')
      ->disableOriginalConstructor()
      ->getMock();

    // Create a mock and queue two responses.
    $mock = new MockHandler([
      $response,
    ]);

    $handler = HandlerStack::create($mock);
    $client = new Client(['handler' => $handler]);

    $connection = new Connection($client, $queue, $logger, $authorization, $siteManagement);

    if (!is_string($url) || !is_string($filter)) {
      $this->setExpectedException(\Exception::class, '$url or $filter is not a string');
    }

    $item = $connection->getHrefLangItem($url, $filter);

    if ($case['title'] === 'Working') {
      $this->assertEquals($case['expected'], $item);
    }
    else {
      $this->assertNull($item);
    }
  }

  /**
   * Test values for the getHrefLangItem method.
   *
   * @return array
   *   The provider array.
   */
  public function getHrefLangItemProvider() {
    return [
      [
        [
          'title' => 'Ivalidate JSON',
          'filter' => '?as',
          'url' => 'http://example.com',
          'response' => new RequestException("Error Communicating with Server", new Request('GET', 'test')),
          'expected' => '',
        ],
      ],
      [
        [
          'title' => 'Ivalidate JSON',
          'filter' => 'fs',
          'url' => 'http://example2.com',
          'response' => new Response(202, ['Content-Length' => 0]),
          'expected' => '',
        ],
      ],
      [
        [
          'title' => 'Ivalidate JSON',
          'filter' => 1,
          'url' => new \stdClass(),
          'response' => new Response(200, ['X-Foo' => 'Bar']),
          'expected' => '',
        ],
      ],
      [
        [
          'title' => 'Ivalidate JSON',
          'filter' => 1,
          'url' => [],
          'response' => new Response(200, ['X-Foo' => 'Bar'], "{'hello' => 'world'"),
          'expected' => '',
        ],
      ],
      [
        [
          'title' => 'Working',
          'filter' => '?zwei',
          'url' => 'http://example.com',
          'response' => new Response(200, ['X-Foo' => 'Bar'], '{"data": [{"world" : "hallo"}]}'),
          'expected' => ['data' => ['world' => 'hallo']],
        ],
      ],
      [
        [
          'title' => 'Ivalidate JSON',
          'filter' => [],
          'url' => 'http://example.com',
          'response' => new Response(200, ['X-Foo' => 'Bar']),
          'expected' => '',
        ],
      ],
      [
        [
          'title' => 'Ivalidate JSON',
          'filter' => new \stdClass(),
          'url' => 'http://example.com',
          'response' => new Response(200, ['X-Foo' => 'Bar']),
          'expected' => '',
        ],
      ],
    ];
  }

  /**
   * @covers ::createHrefLangItemFromEntity
   * @dataProvider getCreateHrefLangItemFromEntityProvider
   */
  public function testCreateHrefLangItemFromEntity($case) {

    $expected = '';
    $entity = '';
    $gid = '';

    $language = $this->getMockBuilder('\Drupal\Core\Language\LanguageInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $entity = $this->getMockBuilder('Drupal\node\Entity\Node')
      ->disableOriginalConstructor()
      ->getMock();

    $url = $this->getMockBuilder('\Drupal\Core\Url')
      ->disableOriginalConstructor()
      ->getMock();
    $entity->expects($this->any())
      ->method('getTitle')
      ->will($this->returnValue($this->getRandomGenerator()->word(10)));

    $entity->expects($this->any())
      ->method('toUrl')->will($this->returnValue($url));

    $entity->expects($this->once())
      ->method('language')
      ->will($this->returnValue($language));

    $immutable_config = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();

    $immutable_config->expects($this->once())
      ->method('get')->with('country.default')
      ->will($this->returnValue('DE'));

    $config_factory = $this->getMockBuilder('Drupal\Core\Config\ConfigFactoryInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $config_factory->expects($this->once())
      ->method('get')->with('system.date')
      ->will($this->returnValue($immutable_config));

    $this->setMockContainerService('config.factory', $config_factory);

    $client = $this->getMockBuilder('GuzzleHttp\ClientInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $queue = $this->getMockBuilder('Drupal\Core\Queue\QueueFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $authorization = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\AuthorizationInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $siteManagement = $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteManagementInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $connection = new Connection($client, $queue, $logger, $authorization, $siteManagement);
    $hreflang = $connection->createHrefLangItemFromEntity($entity, $gid);

    $this->assertEquals($expected, $hreflang);
  }

  /**
   * Test values for the createHrefLangItemFromEntity method.
   *
   * @return array
   *   The provider array.
   */
  public function getCreateHrefLangItemFromEntityProvider() {
    return [
      [['value' => 1]],
      [['value' => 'z']],
      [['value' => '42']],
      [['value' => TRUE]],
      [['value' => FALSE]],
      [
        [
          'value' => $this->getMockBuilder('Drupal\href_lang_exchange\Connection\SiteInterface')
            ->disableOriginalConstructor()
            ->getMock(),
        ],
      ],
    ];
  }

  /**
   * @covers ::checkSelfMaster
   * @dataProvider constructProvider
   */
  public function checkSelfMaster($case) {
  }

  /**
   * @covers ::createHrefLangItem
   * @dataProvider constructProvider
   */
  public function createHrefLangItem($case) {
  }

  /**
   * @covers ::createHrefLangItemAsync
   * @dataProvider constructProvider
   */
  public function createHrefLangItemAsync($case) {
  }

  /**
   * Data provider for testConstruct.
   */
  public function constructProvider() {
    return [
      [['conjunction' => 'AND', 'members' => []]],
      [['conjunction' => 'OR', 'members' => []]],
    ];
  }

  /**
   * Sets up a mock expectation for the container get() method.
   *
   * @param string $service_name
   *   The service name to expect for the get() method.
   * @param mixed $return
   *   The value to return from the mocked container get() method.
   */
  protected function setMockContainerService($service_name, $return = NULL) {
    $expects = $this->container->expects($this->any())
      ->method('get');

    if (isset($return)) {
      $expects->will($this->returnValue($return));
    }
    else {
      $expects->will($this->returnValue(TRUE));
    }

    \Drupal::setContainer($this->container);
  }

}
