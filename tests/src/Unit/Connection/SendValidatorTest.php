<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Connection;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\href_lang_exchange\Connection\Connection;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;
use Drupal\href_lang_exchange\Connection\ResourceInterface;
use Drupal\href_lang_exchange\Connection\SendValidator;
use Drupal\href_lang_exchange\HrefLangItemInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Connection\SendValidator
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class SendValidatorTest extends UnitTestCase {

  /**
   * @covers ::isItJsonApiRequest
   * @dataProvider isItJsonApiRequestProvider
   */
  public function testIsItJsonApiRequest($case) {
    $send_validator = new SendValidator($case['connection'], $case['request_stack'], $case['route_matcher'], $case['resource_manager']);
    $response = $send_validator->isItJsonApiRequest($case['entity']);
    $this->assertEquals($case['expect'], $response);
  }

  /**
   * Test values for the isItJsonApiRequest method.
   *
   * @return array
   *   The provider array.
   */
  public function isItJsonApiRequestProvider() {
    $default_entity = $this->getMockBuilder(HrefLangItemInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection = $this->getMockBuilder(ConnectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection->expects($this->any())
      ->method('checkSelfMaster')
      ->will($this->returnValue(TRUE));

    $default_request_stack = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();

    $basic = new Request();
    $basic->setMethod('POST');
    $basic->server->set('REQUEST_URI', Connection::RESOURCE_ADDRESS);

    $default_request_stack->expects($this->any())
      ->method('getCurrentRequest')
      ->will($this->returnValue($basic));

    $default_route_matcher = $this->getMockBuilder(RouteMatchInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_resource_manager = $this->createResourceManager(Connection::RESOURCE_ADDRESS);

    $default_elements = [
      [
        [
          'entity' => $this->getMockBuilder(EntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock(),
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $this->createConnection(FALSE),
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $this->createResourceManager(TRUE),
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $this->createResourceManager(FALSE),
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $this->createResourceManager(new \stdClass()),
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $this->createResourceManager([]),
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $this->createResourceManager(''),
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $this->createResourceManager(3),
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'POST',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],

      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'PATCH',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'GET',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],

      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 4,
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            ['_create_multiple' => 'FALSE'],
            [],
            [],
            [
              'REQUEST_METHOD' => 'POST',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
    ];

    return $default_elements;
  }

  /**
   * {@inheritdoc}
   */
  public function createConnection($checkSelfMaster) {
    $mock = $this->getMockBuilder(ConnectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $mock->expects($this->once())
      ->method('checkSelfMaster')
      ->will($this->returnValue(FALSE));

    return $mock;
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceManager($getResourceAddress) {
    $mock = $this->getMockBuilder(ResourceInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $mock->expects($this->any())
      ->method('getResourceAddress')
      ->will($this->returnValue($getResourceAddress));

    return $mock;
  }

  /**
   * {@inheritdoc}
   */
  public function createRequestStack($request) {
    $mock = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();

    $mock->expects($this->any())
      ->method('getCurrentRequest')
      ->will($this->returnValue($request));

    return $mock;
  }

  /**
   * @covers ::isItFormEdit
   * @dataProvider isItFormEditProvider
   */
  public function testIsItFormEdit($case) {
    $send_validator = new SendValidator($case['connection'], $case['request_stack'], $case['route_matcher'], $case['resource_manager']);
    $response = $send_validator->isItFormEdit($case['entity']);
    $this->assertEquals($case['expect'], $response);
  }

  /**
   * Test values for the isItFormEdit method.
   *
   * @return array
   *   The provider array.
   */
  public function isItFormEditProvider() {
    $default_entity = $this->getMockBuilder(HrefLangItemInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection = $this->getMockBuilder(ConnectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection->expects($this->any())
      ->method('checkSelfMaster')
      ->will($this->returnValue(TRUE));

    $default_request_stack = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();

    $basic = new Request();
    $basic->setMethod('POST');
    $basic->server->set('REQUEST_URI', Connection::RESOURCE_ADDRESS);

    $default_request_stack->expects($this->any())
      ->method('getCurrentRequest')
      ->will($this->returnValue($basic));

    $default_route_matcher = $this->createRouteMatch('entity.href_lang_item.edit_form');

    $default_resource_manager = $this->createResourceManager(Connection::RESOURCE_ADDRESS);

    $default_elements = [
      [
        [
          'entity' => $this->getMockBuilder(EntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock(),
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $this->createConnection(FALSE),
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'POST',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],

      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'PATCH',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'GET',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],

      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 4,
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            ['_create_multiple' => 'FALSE'],
            [],
            [],
            [
              'REQUEST_METHOD' => 'POST',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(''),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(NULL),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(FALSE),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(4),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch([]),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(new \stdClass()),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
    ];

    return $default_elements;
  }

  /**
   * {@inheritdoc}
   */
  public function createRouteMatch($match) {
    $mock = $this->getMockBuilder(RouteMatchInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $mock->expects($this->any())
      ->method('getRouteName')
      ->will($this->returnValue($match));

    return $mock;
  }

  /**
   * @covers ::isItFormAdd
   * @dataProvider isItFormAddProvider
   */
  public function testIsItFormAdd($case) {
    $send_validator = new SendValidator($case['connection'], $case['request_stack'], $case['route_matcher'], $case['resource_manager']);
    $response = $send_validator->isItFormAdd($case['entity']);
    $this->assertEquals($case['expect'], $response);
  }

  /**
   * Test values for the isItFormAdd method.
   *
   * @return array
   *   The provider array.
   */
  public function isItFormAddProvider() {
    $default_entity = $this->getMockBuilder(HrefLangItemInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection = $this->getMockBuilder(ConnectionInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $default_connection->expects($this->any())
      ->method('checkSelfMaster')
      ->will($this->returnValue(TRUE));

    $default_request_stack = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();

    $basic = new Request();
    $basic->setMethod('POST');
    $basic->server->set('REQUEST_URI', Connection::RESOURCE_ADDRESS);

    $default_request_stack->expects($this->any())
      ->method('getCurrentRequest')
      ->will($this->returnValue($basic));

    $default_route_matcher = $this->createRouteMatch('href_lang_item.href_lang_item_add');

    $default_resource_manager = $this->createResourceManager(Connection::RESOURCE_ADDRESS);

    $default_elements = [
      [
        [
          'entity' => $this->getMockBuilder(EntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock(),
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $this->createConnection(FALSE),
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'POST',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],

      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'PATCH',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 'GET',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],

      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            [],
            [],
            [],
            [
              'REQUEST_METHOD' => 4,
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $this->createRequestStack(new Request(
            [],
            [],
            ['_create_multiple' => 'FALSE'],
            [],
            [],
            [
              'REQUEST_METHOD' => 'POST',
              'REQUEST_URI' => Connection::RESOURCE_ADDRESS,
            ])),
          'route_matcher' => $default_route_matcher,
          'resource_manager' => $default_resource_manager,
          'expect' => TRUE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(''),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(NULL),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(FALSE),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(4),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch([]),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
      [
        [
          'entity' => $default_entity,
          'connection' => $default_connection,
          'request_stack' => $default_request_stack,
          'route_matcher' => $this->createRouteMatch(new \stdClass()),
          'resource_manager' => $default_resource_manager,
          'expect' => FALSE,
        ],
      ],
    ];

    return $default_elements;
  }

}
