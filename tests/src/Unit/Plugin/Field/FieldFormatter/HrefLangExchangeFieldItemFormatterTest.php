<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Plugin\Field\FieldFormatter;

use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Plugin\Field\FieldFormatter\HrefLangExchangeFieldItemFormatter
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangExchangeFieldItemFormatterTest extends UnitTestCase {

  /**
   * @covers ::settingsSummary
   */
  public function testSettingsSummary() {
    $summary = [];
    $summary[] = $this->t('Add the href html tags.');
    return $summary;
  }

  /**
   * @covers ::__construct
   */
  public function testConstruct() {

  }

  /**
   * @covers ::viewElements
   * @dataProvider viewElementsProvider
   */
  public function testViewElements() {
    /* $element = [];
    $head_elememts = [];

    $entity = $items->getEntity();

    try {
    $url = $entity->toUrl();
    $url->setAbsolute(TRUE);
    $url = $url->toString();
    }
    catch (\Exception $e) {
    $this->logger->info("Can't create url :" . $e->getMessage());
    $url = '';
    }

    foreach ($items as $delta => $item) {
    $this->addMetatags($item->value, $head_elememts, []);
    }

    if (!empty($head_elememts)) {
    $element['#attached']['html_head'] = $head_elememts;
    }

    return $element;*/
  }

  /**
   * {@inheritdoc}
   */
  public function buildProvider() {
    return [[['a' => 'n']]];
  }

}
