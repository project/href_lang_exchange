<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Plugin\Field\FieldWidget;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Plugin\Field\FieldWidget\HrefLangExchangeFieldItemAutocompleteWidget
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangExchangeFieldItemAutocompleteWidgetTest extends UnitTestCase {

  /**
   * @covers ::formElement
   */
  public function testFormElement() {

  }

  /**
   * @covers ::getPossibleOptions
   */
  public function testGetPossibleOptions(AccountInterface $account = NULL, $format = 'en') {

  }

  /**
   * @covers ::validate
   */
  public function testValidate() {

  }

}
