<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Plugin\Field\FieldType\HrefLangExchangeFieldItem
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangExchangeFieldItemTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testConstruct() {

  }

  /**
   * @covers ::isEmpty
   */
  public function testIsEmpty() {
    // $hreflang = new HrefLangExchangeFieldItem();
    return FALSE;
  }

  /**
   * @covers ::preSave
   */
  public function preSave() {
    if (empty($this->value)) {
      $this->value = $this->generator->generate();
    }
  }

  /**
   * @covers ::postSave
   */
  public function testPostSave($update) {

    $entity = $this->loadEntity();

    if (
      $entity instanceof EntityPublishedInterface &&
      !$entity->isPublished()
    ) {
      return;
    }

    try {
      $master = $this->connection->getMaster();
      $master_url = $master->getUrl();
      $href_lang_item = $this->connection->createHrefLangItemFromEntity($this->getEntity()->getTranslation($entity->language()->getId()), $this->getValue());

      $item = $href_lang_item->toJsonApi();

      /** @var \Drupal\Core\Entity\EntityInterface $original */
      $original = $this->getEntity()->original;

      // If you translate something the original is the untranslated entity.
      if (
        !empty($original) &&
        $original->hasTranslation($entity->language()->getId())
      ) {
        $original = $original->getTranslation($entity->language()->getId());
      }

      if (
        !empty($original) &&
        $original->language()->getId() === $entity->language()->getId()
      ) {

        $url = $original->toUrl();
        $url->setAbsolute(TRUE);
        $url = $url->toString();

        $response = $this->connection->getHrefLangItem($master_url, '?filter[path]=' . $url . '', [404]);
        if (!empty($response) && isset($response['data']['id'])) {
          $item['data']['id'] = $response[0]['data']['id'];
        }
        // If we have multiple values.
        if (!empty($response['data']) && is_array($response['data'])) {
          $item['data']['id'] = $response['data'][0]['id'];
        }
      }

      // Switch between queue and directly [queue-it].
      $this->connection->createHrefLangItemAsync($master_url, $item);

    }
    catch (\Exception $e) {
      $this->logger->alert("Can't save Field: " . $e->getMessage());
    }

  }

  /**
   * @covers ::delete
   */
  public function testDelete() {
    $entity = $this->getEntity();
    $languages = [];

    try {

      $master = $this->connection->getMaster();
      $master_url = $master->getUrl();

      $href_lang_item = $this->connection->createHrefLangItemFromEntity($entity, $this->getString());

      // The url alias is already deleted at this time.
      $response = $this->connection->getHrefLangItem($master_url, '?filter[gid]=' . $this->getString() . '', [404]);

      if (isset($response['data']) && is_array($response['data'])) {
        $languages[$href_lang_item->getLanguage()] = $href_lang_item->getLanguage();
        if ($entity instanceof TranslatableInterface) {
          $languages = $entity->getTranslationLanguages();
          foreach ($languages as $language) {
            $languages[$language->getId()] = $language->getId();
          }
        }

        $response['data'] = array_filter($response['data'], function ($element) use ($href_lang_item, $languages) {
          // Maybe filter path.
          if (
          in_array($element['attributes']['language'], $languages)
          ) {
            return TRUE;
          }
          return FALSE;
        });

        foreach ($response['data'] as $element) {
          if (isset($element['id'])) {
            $href_lang_item->set('uuid', $element['id']);
            $this->connection->deleteHrefLangItemAsync($master_url, $href_lang_item->toJsonApi());
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->alert('delete() HrefLangExchangeFieldItem:' . $e->getMessage());
    }

  }

}
