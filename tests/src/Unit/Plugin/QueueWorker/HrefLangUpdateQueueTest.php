<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Plugin\QueueWorker;

use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Plugin\QueueWorker\HrefLangUpdateQueue
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLangUpdateQueueTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testConstruct() {

  }

  /**
   * @covers ::processItem
   */
  public function processItem($data) {
    if (
      (!isset($data['url'])) ||
      (!isset($data['values'])) ||
      (!isset($data['type']))
    ) {
      $this->logger
        ->critical('Not well formed href lang request', $data);
    }

    switch ($data['type']) {
      case HrefLangUpdateQueue::CREATE_LANG_ITEM:
        $this->connection->createHrefLangItem($data['url'], $data['values']);
        break;

      case HrefLangUpdateQueue::DELETE_LANG_ITEM:
        $this->connection->deleteHrefLangItem($data['url'], $data['values']);
        break;
    }
  }

}
