<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Plugin\Block;

use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Plugin\Block\HrefLanguageConnectionCountBlock
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class HrefLanguageConnectionCountBlockTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testConstruct() {

  }

  /**
   * @covers ::build
   * @dataProvider buildProvider
   */
  public function testBuild() {

  }

  /**
   * Build providers.
   */
  public function buildProvider() {
    return [[['a' => 'n']]];
  }

}
