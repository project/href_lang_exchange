<?php

namespace Drupal\Tests\href_lang_exchange\Unit\Plugin\views\filter;

use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\href_lang_exchange\Plugin\views\filter\FilterByHrefLangItems
 * @group href_lang_exchange
 * @group legacy
 *
 * @internal
 */
class FilterByHrefLangItemsTest extends UnitTestCase {

  /**
   * @covers ::init
   */
  public function testInit() {
    /* ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL
    parent::init($view, $display, $options);
    $this->definition['options callback'] = [$this, 'generateOptions'];*/
  }

  /**
   * @covers ::opNoGroup
   */
  public function testOpNoGroup() {
    $this->ensureMyTable();

    $items = \Drupal::entityTypeManager()
      ->getStorage('href_lang_item')
      ->getAllUnconnectedItems();

    $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", array_values($items), 'IN');

  }

  /**
   * @covers ::validate
   */
  public function testValidate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * @covers ::generateOptions
   */
  public function testGenerateOptions() {
    return [];
  }

  /**
   * @covers ::operators
   */
  public function testOperators() {
    $operators = parent::operators();
    $operators['only_single'] = [
      'title' => $this->t('Has no group'),
      'short' => $this->t('no_group'),
      'short_single' => $this->t('NG'),
      'method' => 'opNoGroup',
      'values' => 0,
    ];

    return $operators;
  }

}
