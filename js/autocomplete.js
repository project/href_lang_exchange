/**
 * @file
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    Drupal.behaviors.autocomplete_href_lang = {
        attach: function (context, settings) {
            $('.field--type-href-lang-exchange-item', document).once('autocomplete').each(function() {
              let button_handler = function (event) {
                  document.querySelector(".current_active_item").innerHTML = '';
  
                  document.querySelector(".href_lang_gid_value").value = '';
                  document.querySelector(".autocomplete_href_lang_filter_lang").value = '';
                  document.querySelector(".autocomplete_href_lang").value = '';
  
                  document.querySelector(".autocomplete_href_lang ").classList.remove("search_off");
                  document.querySelector(".autocomplete_href_lang_filter_lang").classList.remove("search_off");
                  document.querySelector("#autoComplete_list").classList.remove("search_off");
  
                  event.preventDefault();
              };
  
              async function loadOptionsForLanguageFilter() {
                  document.querySelector('.autocomplete_href_lang_filter_lang').options.length = 0;
                  const source = await fetch(`/autocomplete/search_facet`);
                  const items = await source.json();
  
                  var option = document.createElement("option");
                  option.text = Drupal.t('- None -');
                  option.value = '';
                  document.querySelector('.autocomplete_href_lang_filter_lang').add(option);
  
                  for (var prop in items) {
                      var option = document.createElement("option");
                      option.text = items[prop];
                      option.value = prop;
                      document.querySelector('.autocomplete_href_lang_filter_lang').add(option);
                  }
              }
  
              loadOptionsForLanguageFilter();
  
              const render_item = function (match, objects, button_label, flag = '') {
                  let list_of_items = '';
                  let additional_class = '';
                  objects.forEach(
                      function (object, index) {
                          let title_sub = object.title.substr(0, settings.href_lang_exchange.text_max_length);
                          if (title_sub.length !== object.title.length) {
                              title_sub += '...';
                          }
  
                          if (object.short_title) {
                              object.region = object.short_title;
                          }
  
                          list_of_items +=
                              '<div class="other_href_items">' +
                              '<span class="flag-icon flag-icon-squared flag-icon-' + object.region.toLowerCase() + '" title="' + object.language_label + '"></span>' +
                              '<p class="title_other"><a href="' + object.url + '" target="_blank">' + title_sub + '</a></p>' +
                              '</div>';
                      }
                  );
  
                  if (objects.length === 0) {
                      additional_class = 'alone';
                  }
  
                  let title_main = match.title.substr(0, settings.href_lang_exchange.text_max_length);
                  if (title_main.length !== match.title.length) {
                      title_main += '...';
                  }
  
                  if (match.short_title) {
                      match.region = match.short_title;
                  }
  
                  return (
                      '<div class="result_item ' + flag + '">' +
                      '<div class="current_item ' + additional_class + '">' +
                      '<div class="selected_element">' +
                      '<span class="flag-icon flag-icon-squared flag-icon-' + match.region.toLowerCase() + '" title="' + match.language_label.toLowerCase() + '"></span>' +
                      '<p class="title"><a href="' + match.url + '" target="_blank">' + title_main + '</a></p>' + '</div>' +
                      '<div class="list_other_href_items">' + list_of_items + '</div>' + '</div>' +
                      '<button class="button button--primary">' + button_label + '</button>' +
                      '</div>');
              };
  
              let active_item_button = document.querySelector('.current_active_item button');
              if (active_item_button) {
                  active_item_button.addEventListener("click", button_handler);
              }
  
              document.querySelector('.autocomplete_href_lang_filter_lang').addEventListener("change", function (event) {
                  document.querySelector(".autocomplete_href_lang").dispatchEvent(new Event('focusout'));
                  event.preventDefault();
              });

                const autoCompletejs = new autoComplete(
                  {
                      data: {
                          src: async function () {
  
                              document.querySelector(".autocomplete_href_lang ").classList.add('ui-autocomplete-loading');
  
                              const query = document.querySelector(".autocomplete_href_lang ").value;
                              const lang = document.querySelector('.autocomplete_href_lang_filter_lang').value;
                              const site = settings.href_lang_exchange.site;
                              const sub_lang = settings.href_lang_exchange.sub_lang;
  
                              const ignore = settings.href_lang_exchange.ignore_elements;
  
                              const source = await fetch(`${settings.href_lang_exchange.autocomplet_adress}?q=${query}&lang=${lang}&sub_lang=${sub_lang}&site=${site}&ignore=${ignore}`);
  
                              return await source.json();
                          },
                          cache: false,
                      },
                      trigger: {
                          event: ["input", "focusin", "focusout"]
                      },
                      placeHolder: "Search for href",
                      selector: ".autocomplete_href_lang",
                      searchEngine: function (query, record) {
                          return record;
                      },
                      highlight: true,
                      resultsList: {
                          render: true,
                          destination: document.querySelector("#autoComplete"),
                          navigation: function (event, input, resultsList, feedback, resultsValues) {
                              var dataAttribute = "data-id";
  
                              var li = resultsList.childNodes;
  
                              li.forEach((selection) => {
                                  const button = selection.querySelector("button");
                                  button.onclick = (event) => {
                                      event.preventDefault();
                                      feedback({
                                          event: event,
                                          query: input instanceof HTMLInputElement ? input.value : input.innerHTML,
                                          matches: resultsValues.matches,
                                          results: resultsValues.list.map(function (record) {
                                              return record.value;
                                          }),
                                          selection: resultsValues.list.find(function (value) {
                                              if (event.type === "click") {
                                                  return value.index === Number(selection.getAttribute(dataAttribute));
                                              }
                                          })
                                      });
                                      resultsList.innerHTML = "";
                                  };
                              });
                          }
                      },
                      resultItem: {
                          content: (data, source) => {
                              const label = Drupal.t('Connect');
                              source.innerHTML = render_item(data.match, data.value.elements, label, '');
                              document.querySelector(".autocomplete_href_lang ").classList.remove('ui-autocomplete-loading');
                          },
                          element: "div",
                      },
                      noResults: function () {
                          document.querySelector(".autocomplete_href_lang ").classList.remove('ui-autocomplete-loading');
                      },
                      onSelection: function (feedback) {
  
                          const selection = feedback.selection.value.gid;
  
                          document.querySelector(".autocomplete_href_lang ").classList.add("search_off");
                          document.querySelector(".autocomplete_href_lang_filter_lang").classList.add("search_off");
                          document.querySelector(".href_lang_gid_value").value = selection;
  
                          const label = Drupal.t('Disconnect');
                          const selected_item = render_item(feedback.selection.match, feedback.selection.value.elements, label, 'current_active_item');
  
                          document.querySelector(".current_active_item").innerHTML = selected_item;
                          document.querySelector("#autoComplete_list").classList.add("search_off");
                          document.querySelector('.current_active_item button').addEventListener("click", button_handler)
  
                      },
                  }
                );
            });

        }
    };

})(jQuery, Drupal, drupalSettings);
