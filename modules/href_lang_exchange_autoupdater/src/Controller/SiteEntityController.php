<?php

namespace Drupal\href_lang_exchange_autoupdater\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\Diactoros\Response;

/**
 * Class SiteEntityController.
 *
 *  Returns responses for Site entity routes.
 */
class SiteEntityController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new SiteEntityController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function handle() {
    /** @var \Drupal\href_lang_exchange\Connection\ConnectionInterface $connection */
    $connection = \Drupal::service('href_lang_exchange.connection');

    /** @var \Drupal\href_lang_exchange_href\SiteEntityStorageInterface $site_storage */
    $site_storage = \Drupal::entityTypeManager()
      ->getStorage('site_entity');

    /** @var \Drupal\href_lang_exchange\HrefLangItemStorageInterface $href_storage */
    $href_storage = \Drupal::entityTypeManager()
      ->getStorage('href_lang_item');

    $sites = $site_storage->loadAllPublishedDestinationSites();

    foreach ($sites as $site) {
      $filter = '?filter[a-label][condition][path]=path&filter[a-label][condition][operator]=STARTS_WITH&filter[a-label][condition][value]=' . $site->getUrl();
      $response = $connection->getHrefLangItem($site->getUrl(), $filter, [404]);
      foreach ($response['data'] as $element) {
        $attributes = $element['attributes'];

        $id = $attributes['drupal_internal__id'] ?? '';
        unset($attributes['drupal_internal__id']);
        if (!empty($id)) {
          $attributes['id'] = $id;
        }
        $uuid = $element['id'] ?? '';

        $entity = NULL;

        if (!empty($uuid)) {
          $entity = \Drupal::service('entity.repository')
            ->loadEntityByUuid('href_lang_item', $uuid);
          foreach ($attributes as $key => $value) {
            $entity->set($key, $value);
          }

        }

        if (empty($entity)) {
          $entity = $href_storage->create($attributes);
        }

        $entity->save();
      }
    }

    return new Response();
  }

}
