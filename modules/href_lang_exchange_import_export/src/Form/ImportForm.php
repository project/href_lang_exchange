<?php

namespace Drupal\href_lang_exchange_import_export\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\href_lang_exchange_import_export\Services\CsvManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ImportForm class implementation.
 */
class ImportForm extends FormBase {

  /**
   * Filesystem to handle files.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The CsvManager.
   *
   * @var \Drupal\href_lang_exchange_import_export\Services\CsvManagerInterface
   */
  protected CsvManagerInterface $csvManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The Filesystem.
   * @param \Drupal\href_lang_exchange_import_export\Services\CsvManagerInterface $csvManager
   *   The CsvManager.
   */
  public function __construct(FileSystemInterface $fileSystem, CsvManagerInterface $csvManager) {
    $this->fileSystem = $fileSystem;
    $this->csvManager = $csvManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('href_lang_exchange_import_export.csv_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'href_lang_exchange_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [
      '#attributes' => ['enctype' => 'multipart/form-data'],
    ];

    $form['file_upload_details'] = [
      '#markup' => $this->t('<b>The File</b>'),
    ];

    $validators = [
      'file_validate_extensions' => ['csv'],
    ];
    $form['csv_file'] = [
      '#type' => 'managed_file',
      '#name' => 'csv_file',
      '#title' => $this->t('File *'),
      '#size' => 20,
      '#description' => $this->t('CSV format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'temporary://href_lang_csv',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('csv_file') == NULL) {
      $form_state->setErrorByName('csv_file', $this->t('File.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $csv_dir = 'tmp:';

    $this->fileSystem->prepareDirectory($csv_dir, FileSystemInterface::CREATE_DIRECTORY);

    $fid = $form_state->getValue('csv_file')[0];

    $this->csvManager->generateCsvFromFile($fid);

    $form_state->setRedirect('view.href_lang_item_list.list');
  }

}
