<?php

namespace Drupal\href_lang_exchange_import_export\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\href_lang_exchange_import_export\Services\CsvManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ExportForm class implementation.
 */
class ExportForm extends ConfirmFormBase {

  /**
   * The CsvManager.
   *
   * @var \Drupal\href_lang_exchange_import_export\Services\CsvManagerInterface
   */
  protected CsvManagerInterface $csvManager;

  /**
   * The constructor.
   *
   * @param \Drupal\href_lang_exchange_import_export\Services\CsvManagerInterface $csvManager
   *   The CsvManagerInterface.
   */
  public function __construct(CsvManagerInterface $csvManager) {
    $this->csvManager = $csvManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('href_lang_exchange_import_export.csv_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'href_lang_exchange_export';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $url = new Url('view.href_lang_item_list.list');
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to create a file with all groups?');
  }

}
