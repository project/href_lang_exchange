<?php

namespace Drupal\href_lang_exchange_import_export\Services;

/**
 * The CsvManagerInterface definition.
 */
interface CsvManagerInterface {

  /**
   * Use CSV file as source of true to create entities.
   *
   * @param int $fid
   *   The CSV file.
   */
  public function generateCsvFromFile(int $fid);

}
