<?php

namespace Drupal\href_lang_exchange_import_export\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\href_lang_exchange\Entity\HrefLangItem;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\href_lang_exchange\Connection\ConnectionInterface;

/**
 * CsvManager class implementation.
 */
class CsvManager implements CsvManagerInterface {
  use StringTranslationTrait;

  const GROUPS_ID_HEADER = 'Group IDs';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Messenger service.
   *
   * @var \Drupal\href_lang_exchange\Connection\ConnectionInterface
   */
  protected $hreflangExchangeConnection;

  /**
   * Constructs a new CsvManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\href_lang_exchange\Connection\ConnectionInterface $connection
   *   The href_lang_exchange connection service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, MessengerInterface $messenger, ConnectionInterface $connection) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
    $this->hreflangExchangeConnection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function generateCsvFromFile(int $fid) {
    // Pause connection during the import.
    $this->hreflangExchangeConnection->pauseSyncTemporary();

    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager->getStorage('file')->load($fid);

    // https://www.php.net/manual/es/function.str-getcsv.php#117692
    $csv = array_map('str_getcsv', file($file->getFileUri()));
    array_walk($csv, function (&$a) use ($csv) {
      $a = array_combine($csv[0], $a);
    });
    array_shift($csv);

    // As we only update existing items we get all to search for changes.
    /** @var Drupal\href_lang_exchange\Entity\HrefLangItem[] $entities */
    $entities = $this->entityTypeManager->getStorage('href_lang_item')->loadMultiple();

    // @todo Move this for into a drupal queue.
    // Check current entities to update or remove.
    foreach ($entities as $key => $hrefLanItem) {
      // If the item is not in CSV, remove.
      if ($this->removeItemIfNotExists($hrefLanItem, $csv)) {
        unset($entities[$key]);
      }
    }

    // Check new entities comming from CSV.
    foreach ($csv as $group) {
      $groupId = $group[$this::GROUPS_ID_HEADER];
      foreach ($group as $code => $url) {
        // Skip empty cells.
        if (!$url) {
          continue;
        }

        // Skip header.
        if ($code == $this::GROUPS_ID_HEADER) {
          continue;
        }

        // Check if we already have the entity.
        $languageCountry = $this->decodeHeader($code);
        if (!$languageCountry) {
          // If there's any error in the header format continue.
          continue;
        }

        $entityId = $this->checkItemByGroupAndCode($entities, $languageCountry['country'], $languageCountry['language'], $groupId);
        if ($entityId && $entities[$entityId]->getPath() == $url) {
          // If already exists, there's nothing to do.
          continue;
        }

        // Create the entity if doesn't exist.
        if (!$entityId) {
          $this->createEntityByUrlAndCode($url, $languageCountry['country'], $languageCountry['language'], $groupId);
        }
        // Update if exists but the URL is different.
        else {
          $entities[$entityId]->setPath($url)->save();
        }
      }
    }

    // Allow sync to continue.
    $this->hreflangExchangeConnection->resumeSync();
  }

  /**
   * Create an entity with params known by csv.
   *
   * @param string $url
   *   The url.
   * @param string $country
   *   The country.
   * @param string $language
   *   The language.
   * @param string $group
   *   The group.
   */
  private function createEntityByUrlAndCode(string $url, string $country, string $language, string $group) {
    // Check if url matches with any of the current site entities.
    $site = $this->checkSiteByCountryAndLanguage($country, $language);

    if ($site === FALSE) {
      $this->messenger->addWarning($this->t('The item for the group %group in column %column could not be created as there are no matching sites.', [
        '%group' => $group,
        '%column' => $this->encodeHeader($language, $country),
      ]));
    }

    // @see https://www.drupal.org/project/drupal/issues/1867228
    HrefLangItem::Create([
      'title' => $this->encodeHeader($country, $language),
      'region' => $country,
      'language' => $language,
      'gid' => $group,
      'path' => $url,
      'site_entity' => 1,
    ])->save();

  }

  /**
   * Check if any of the existing entities matches with given parameters.
   *
   * @param array $entities
   *   The entities.
   * @param string $country
   *   The country.
   * @param string $language
   *   The language.
   * @param string $group
   *   The group.
   */
  private function checkItemByGroupAndCode(array $entities, string $country, string $language, string $group) {
    $currentEntities = $this->entitiesToArray(($entities));

    foreach ($currentEntities as $id => $entity) {
      if ($entity['country'] == $country && $entity['language'] == $language && $entity['group'] == $group) {
        return $id;
      }
    }

    return FALSE;

  }

  /**
   * Convert all entities to an array to improve performance.
   *
   * @param array $entities
   *   The entities.
   *
   * @return array
   *   The list of entities.
   */
  private function entitiesToArray(array $entities) {
    $list = &drupal_static(__FUNCTION__);
    if (isset($list)) {
      return $list;
    }
    $list = [];
    foreach ($entities as $id => $entity) {
      $list[$id] = [
        'url' => $entity->getPath(),
        'country' => $entity->getRegion(),
        'language' => $entity->getLanguage(),
        'group' => $entity->getGid(),
      ];
    }

    return $list;
  }

  /**
   * Convert a CSV header into an array with region and language.
   *
   * @param string $code
   *   The entities.
   *
   * @return array
   *   The encoded array['language', 'country].
   */
  private function decodeHeader(string $code) {
    $explodedCode = explode('-', $code);
    if (!is_array($explodedCode) || count($explodedCode) != 2) {
      $this->messenger->addWarning($this->t('Issue with header %header found, format should be language-country', [
        '%header' => $code,
      ]));
      return FALSE;
    }

    return [
      'language' => $explodedCode[0],
      'country' => $explodedCode[1],
    ];
  }

  /**
   * Encode the CSV column header for a given language and country.
   */
  private function encodeHeader(string $language, string $country) {
    return $language . '-' . $country;
  }

  /**
   * Return the group id for a given path and a column key on CSV.
   *
   * @param string $path
   *   The path.
   * @param string $columnKey
   *   The column key.
   * @param array $csv
   *   The csv file moved to array.
   *
   * @return string
   *   The group id.
   */
  private function searchGroupIdByPath(string $path, string $columnKey, array $csv) {
    $key = array_search($path, array_column($csv, $columnKey));
    if ($key === FALSE) {
      return FALSE;
    }
    return $csv[$key]['Group IDs'];

  }

  /**
   * Calculate the columnkey in csv for a given HrefLangItem.
   *
   * @param \Drupal\href_lang_exchange\Entity\HrefLangItem $item
   *   The HrefLangItem.
   *
   * @return string
   *   The key.
   */
  private function calculateColumnKey(HrefLangItem $item) {
    $key = $item->getLanguage() . '-' . $item->getRegion();
    if (!isset($this->languageHeaders[$key])) {
      $this->languageHeaders[$key] = $key;
    }

    $this->messenger->addWarning($this->t('The item %id was removed as the URL %url could not be found in CSV file.', [
      '%id' => $item->id(),
      '%url' => $item->getPath(),
    ]));

    return $key;
  }

  /**
   * Search an item in CSV and remove if does not exist.
   *
   * @param \Drupal\href_lang_exchange\Entity\HrefLangItem $item
   *   The item.
   * @param array $csv
   *   The CSV.
   *
   * @return bool
   *   TRUE if the item has been removed.
   */
  private function removeItemIfNotExists(HrefLangItem $item, array $csv) {
    $groupKey = array_search($item->getGid(), array_column($csv, $this::GROUPS_ID_HEADER));
    if ($groupKey === FALSE) {
      $this->entityTypeManager->getStorage('href_lang_item')->delete([$item]);
      $this->messenger->addMessage($this->t('The item %id was removed as the group %gid could not be found in CSV file.', [
        '%id' => $item->id(),
        '%gid' => $item->getGid(),
      ]));
      return TRUE;
    }

    $header = $this->encodeHeader($item->getLanguage(), $item->getRegion());
    if (!$csv[$groupKey][$header]) {
      $this->entityTypeManager->getStorage('href_lang_item')->delete([$item]);
      $this->messenger->addMessage($this->t('The item %id was removed as the URL %url could not be found in CSV file.', [
        '%id' => $item->id(),
        '%url' => $item->getPath(),
      ]));
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get a list of all urls in CSV to remove items if not in list.
   *
   * @param array $csv
   *   The multi dimensional array with urls.
   *
   * @return string
   *   The list.
   */
  private function getUrllist(array $csv) {
    $list = &drupal_static(__FUNCTION__);
    if (isset($list)) {
      return $list;
    }
    $list = [];
    foreach ($csv as $index => $row) {
      // Skip headers.
      unset($row[0]);

      $list = array_merge($list, array_values($row));
    }
    return $list;
  }

  /**
   * Get a site for a given country(region) and languge.
   *
   * @param string $country
   *   The region.
   * @param string $language
   *   The language.
   *
   * @return mixed
   *   The site metadata or False if couldn't find.
   */
  private function checkSiteByCountryAndLanguage(string $country, string $language) {
    $sites = $this->getAllSitesEncoded();

    foreach ($sites as $key => $site) {
      if ($site['country'] == $country && $site['language'] == $language) {
        return $site;
      }
    }

    return FALSE;
  }

  /**
   * Generates an array with all sites.
   *
   * @return array
   *   An array with all sites.
   */
  private function getAllSitesEncoded() {
    $sites = &drupal_static(__FUNCTION__);
    if (isset($sites)) {
      return $sites;
    }
    /** @var Drupal\href_lang_exchange_href\EntitySiteEntityInterface[] $entities */
    $entities = $this->entityTypeManager->getStorage('site_entity')->loadMultiple();
    foreach ($entities as $entity) {
      $header = $this->encodeHeader($entity->field_language->value, $entity->field_country->value);
      $sites[$header] = [
        'country' => $entity->field_country->value,
        'language' => $entity->field_language->value,
        'url' => $entity->url->value,
        'id' => $entity->id(),
      ];
    }
    return $sites;
  }

}
