<?php

namespace Drupal\href_lang_exchange_import_export\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\href_lang_exchange\Entity\HrefLangItem;
use Symfony\Component\HttpFoundation\Response;

/**
 * An example controller.
 */
class DownloadFileController extends ControllerBase {

  /**
   * Headers of all languages used in the CSV.
   *
   * @var array
   */
  private array $languageHeaders;

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    // Create the handle to put all the data.
    $handle = fopen('php://temp', 'w+');

    // Get al entities.
    $items = $this->entityTypeManager()->getStorage('href_lang_item')->loadMultiple();

    // Build an array with all the information grouped.
    foreach ($items as $item) {
      $data[$item->getGid()][$this->buildKey($item)] = $item->getPath();
    }

    // Put the header including the Gid column and all languages.
    fputcsv($handle, array_merge(['Group IDs'], $this->languageHeaders));

    // Fill all groups with.
    foreach ($data as $group => $urls) {
      $row = $this->buildRow($urls);
      fputcsv($handle, array_merge([$group], $row));
    }

    // Rewind the handle to acvoid errors on download.
    rewind($handle);

    // Get CSV encoded data.
    $csv_data = stream_get_contents($handle);

    // Close the file to release resources on OS.
    fclose($handle);

    // Create the response with the file and the headers.
    $response = new Response();

    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="redirection-report.csv"');

    $response->setContent($csv_data);

    return $response;
  }

  /**
   * Fetches data and builds CSV row.
   *
   * @param array $items
   *   HrefLangItem item.
   *
   * @return array
   *   Row data.
   */
  private function buildRow(array $items) {
    foreach ($this->languageHeaders as $language) {
      $data[] = $items[$language] ?? '';
    }

    return $data;
  }

  /**
   * Buids the key based on the region and language.
   *
   * @param Drupal\href_lang_exchange\Entity\HrefLangItem $item
   *   HrefLangItem item.
   *
   * @return array
   *   Row data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildKey(HrefLangItem $item) {
    $key = $item->getLanguage() . '-' . $item->getRegion();
    if (!isset($this->languageHeaders[$key])) {
      $this->languageHeaders[$key] = $key;
    }

    return $key;
  }

}
