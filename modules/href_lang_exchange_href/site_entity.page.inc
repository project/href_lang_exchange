<?php

/**
 * @file
 * Contains site_entity.page.inc.
 *
 * Page callback for Site entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Site entity templates.
 *
 * Default template: site_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_site_entity(array &$variables) {
  // Fetch SiteEntity Entity Object.
  $site_entity = $variables['elements']['#site_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
