<?php

namespace Drupal\href_lang_exchange_href\Authorization;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\href_lang_exchange\Connection\AuthorizationInterface;

/**
 * Provide Authorization Token.
 *
 * @package Drupal\href_lang_exchange_href\Authorization
 */
class AuthorizationToken implements AuthorizationInterface {

  /**
   * The configuration object for site entities.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthHeader() {
    $token = $this->configFactory->get('href_lang_exchange_href.settings')->get('key');
    return 'CDT ' . md5($token);
  }

  /**
   * {@inheritdoc}
   */
  public function sendAuthHeader() {
    return TRUE;
  }

}
