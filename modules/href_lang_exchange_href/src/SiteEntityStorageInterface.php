<?php

namespace Drupal\href_lang_exchange_href;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\href_lang_exchange_href\Entity\SiteEntityInterface;

/**
 * Defines the storage handler class for Site entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Site entity entities.
 *
 * @ingroup href_lang_exchange_href
 */
interface SiteEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Site entity revision IDs for a specific Site entity.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface[]
   *   A list of SiteEntities.
   */
  public function loadAllDestinationSites();

  /**
   * Gets a list of Site entity revision IDs for a specific Site entity.
   *
   * @param \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface $entity
   *   The Site entity entity.
   *
   * @return int[]
   *   Site entity revision IDs (in ascending order).
   */
  public function revisionIds(SiteEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Site entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Site entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Gets a list of revision IDs having a given user as Site entity author.
   *
   * @param string $url
   *   The user entity.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   *   The matched SiteEntities.
   */
  public function findNearestUrlPrefixMatch($url);

  /**
   * Gets a list of Site entity revision IDs for a specific Site entity.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface[]
   *   A list of SiteEntities.
   */
  public function loadAllPublishedDestinationSites();

  /**
   * Gets a list of Site entity revision IDs for a specific Site entity.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface[]
   *   A list of SiteEntities.
   */
  public function loadAllUnpublishedDestinationSites();

}
