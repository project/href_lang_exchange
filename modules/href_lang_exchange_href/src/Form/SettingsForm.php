<?php

namespace Drupal\href_lang_exchange_href\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'href_lang_exchange_href.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'href_lang_exchange_href_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('This is the api endpoint url'),
      '#description' => $this->t('Please add the api endpoint url in this field (Master Url)'),
      '#default_value' => $config->get('api_endpoint'),
    ];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('This is the authentication key for the page'),
      '#description' => $this->t('Please add the uuid key in this field'),
      '#default_value' => $config->get('key'),
    ];

    $form['api_endpoint_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate the queue for the master site'),
      '#description' => $this->t('All request to the master server will be queued.'),
      '#default_value' => $config->get('api_endpoint_queue'),
    ];

    $form['client_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate the queue for the client sites'),
      '#description' => $this->t('All request from the client to the master server will be queued.'),
      '#default_value' => $config->get('client_queue'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);
    if (!UrlHelper::isValid($form_state->getValue('api_endpoint'))) {
      $form_state->setError($form, $this->t('The Url is not valid.'));
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('key', $form_state->getValue('key'))
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('api_endpoint_queue', $form_state->getValue('api_endpoint_queue'))
      ->set('client_queue', $form_state->getValue('client_queue'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
