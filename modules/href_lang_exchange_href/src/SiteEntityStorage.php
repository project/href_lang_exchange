<?php

namespace Drupal\href_lang_exchange_href;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\href_lang_exchange_href\Entity\SiteEntityInterface;

/**
 * Defines the storage handler class for Site entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Site entity entities.
 *
 * @ingroup href_lang_exchange_href
 */
class SiteEntityStorage extends SqlContentEntityStorage implements SiteEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadAllDestinationSites() {
    return $this->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function findNearestUrlPrefixMatch($url) {
    $url_base = parse_url($url, PHP_URL_HOST);
    if (!empty($url_base)) {
      $result = $this->getQuery()
        ->condition('url', $url_base, 'CONTAINS')
        ->execute();

      $result = $this->loadMultiple($result);

      if (!empty($result) && is_array($result)) {
        $result = array_shift($result);
        return $result;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function revisionIds(SiteEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {site_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {site_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllPublishedDestinationSites() {
    $result = $this->getQuery()
      ->condition('status', '1')
      ->execute();

    $result = $this->loadMultiple($result);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllUnpublishedDestinationSites() {
    $result = $this->getQuery()
      ->condition('status', '0')
      ->execute();

    $result = $this->loadMultiple($result);
    return $result;
  }

}
