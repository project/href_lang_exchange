<?php

namespace Drupal\href_lang_exchange_href\Helper;

use GuzzleHttp\ClientInterface;

/**
 * Provide Check Availability Service.
 *
 * @package Drupal\href_lang_exchange_href\Helper
 */
class CheckAvailabilityService implements CheckAvailabilityInterface {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * CheckAvailabilityService constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The http client.
   */
  public function __construct(ClientInterface $client) {
    $this->httpClient = $client;
  }

  /**
   * {@inheritDoc}
   */
  public function hasValidateSitemap($url) {

    $return_value = FALSE;
    $body = '';

    $sitemap = '/sitemap.xml';
    try {
      $response = $this->httpClient->request('GET', $url . $sitemap, ['timeout' => 3]);
      $body = (string) $response->getBody();

    }
    catch (\Exception $e) {
      \Drupal::logger('href_lang_exchange_href')->info($e->getMessage());
    }

    if (!empty($body) && $this->isValidXml($body)) {
      $return_value = TRUE;
    }
    else {
      \Drupal::logger('href_lang_exchange_href')->info('The sitemap.xml is not valid.');
    }

    return $return_value;

  }

  /**
   * {@inheritDoc}
   */
  public function isSiteAvailable($url) {
    $return_value = TRUE;
    try {
      $response = $this->httpClient->request('GET', $url, ['timeout' => 3]);
      $body = (string) $response->getBody();

    }
    catch (\Exception $e) {
      \Drupal::logger('href_lang_exchange_href')->info($e->getMessage());
      $return_value = FALSE;
    }

    return $return_value;
  }

  /**
   * {@inheritDoc}
   */
  private function isValidXml($content) {
    $content = trim($content);
    if (empty($content)) {
      return FALSE;
    }
    // Html go to hell!
    if (stripos($content, '<!DOCTYPE html>') !== FALSE) {
      return FALSE;
    }

    libxml_use_internal_errors(TRUE);
    simplexml_load_string($content);
    $errors = libxml_get_errors();
    libxml_clear_errors();

    return empty($errors);
  }

  /**
   * {@inheritDoc}
   */
  public function isHttpsActive($url) {
    $cert = NULL;

    $stream = stream_context_create(["ssl" => ["capture_peer_cert" => TRUE]]);
    $read = fopen($url, "rb", FALSE, $stream);

    if (!$read) {
      return FALSE;
    }

    $params = stream_context_get_params($read);

    // Check that SSL certificate is not null
    // $cert should be for example "resource(4) of type (OpenSSL X.509)".
    if (isset($params["options"]["ssl"]["peer_certificate"])) {
      $cert = $params["options"]["ssl"]["peer_certificate"];
    }
    $result = (!is_null($cert)) ? TRUE : FALSE;
    return $result;
  }

}
