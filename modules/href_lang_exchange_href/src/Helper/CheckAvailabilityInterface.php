<?php

namespace Drupal\href_lang_exchange_href\Helper;

/**
 * Interface for Check Availability.
 *
 * @package Drupal\href_lang_exchange_href\Helper
 */
interface CheckAvailabilityInterface {

  /**
   * A method to check the availability of a sitemap.
   *
   * @return bool
   *   Return true if it has a sitmap.
   */
  public function hasValidateSitemap($url);

  /**
   * A Method to check site availability.
   *
   * @return bool
   *   Return true if the site is available.
   */
  public function isSiteAvailable($url);

  /**
   * A method to check https.
   *
   * @return bool
   *   Return true if the site has active https.
   */
  public function isHttpsActive($url);

}
