<?php

namespace Drupal\href_lang_exchange_href\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "CheckWebsiteAvailability",
 *   label = @Translation("Check if the master href instances is available", context = "Validation"),
 *   type = "string"
 * )
 *
 * can be removed.
 */
class CheckWebsiteAvailability extends Constraint {

  /**
   * The answer.
   *
   * @var string
   */
  public $notInteger = '%value is not an integer';

  /**
   * The answer.
   *
   * @var string
   */
  public $notUnique = '%value is not unique';

}
