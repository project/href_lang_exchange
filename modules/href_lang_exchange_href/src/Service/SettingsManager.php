<?php

namespace Drupal\href_lang_exchange_href\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\href_lang_exchange\Service\SettingsManagerInterface;

/**
 * Provide Settings Manager.
 *
 * @package Drupal\href_lang_exchange_href\Service
 */
class SettingsManager implements SettingsManagerInterface {

  /**
   * The configuration object for site entities.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function isAdminQueued() {
    return $this->configFactory->get('href_lang_exchange_href.settings')->get('api_endpoint_queue');
  }

  /**
   * {@inheritdoc}
   */
  public function isClientQueued() {
    return $this->configFactory->get('href_lang_exchange_href.settings')->get('client_queue');
  }

}
