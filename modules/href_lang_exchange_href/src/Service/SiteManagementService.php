<?php

namespace Drupal\href_lang_exchange_href\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\href_lang_exchange\Connection\SiteManagementInterface;
use Drupal\href_lang_exchange_href\SiteEntityStorageInterface;

/**
 * Provide Site Management Service.
 *
 * @package Drupal\href_lang_exchange_href\Service
 */
class SiteManagementService implements SiteManagementInterface {

  /**
   * The site entity storage.
   *
   * @var \Drupal\href_lang_exchange_href\SiteEntityStorageInterface
   */
  protected $storage;

  /**
   * The configuration object for site entities.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * SiteManagementService constructor.
   *
   * @param \Drupal\href_lang_exchange_href\SiteEntityStorageInterface $storage
   *   The storage for this service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(SiteEntityStorageInterface $storage, ConfigFactoryInterface $config_factory) {
    $this->storage = $storage;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllDestinationSites() {
    return $this->storage->loadAllDestinationSites();
  }

  /**
   * {@inheritdoc}
   */
  public function getMasterSite() {
    $url = $this->configFactory->get('href_lang_exchange_href.settings')
      ->get('api_endpoint');
    if (!empty($url)) {
      return new MasterElement($url);
    }
    return [];
  }

}
