<?php

namespace Drupal\href_lang_exchange_href\Service;

use Drupal\href_lang_exchange\Connection\SiteInterface;

/**
 * Provide Master Element.
 *
 * @package Drupal\href_lang_exchange_href\Service
 */
class MasterElement implements SiteInterface {

  /**
   * The url of the master instance.
   *
   * @var string
   */
  protected $url;

  /**
   * MasterElement constructor.
   *
   * @param string $url
   *   The url to the master endpoint.
   */
  public function __construct($url) {
    $this->url = $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->url;
  }

}
