<?php

namespace Drupal\href_lang_exchange_href\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Site entity entity.
 *
 * @ingroup href_lang_exchange_href
 *
 * @ContentEntityType(
 *   id = "site_entity",
 *   label = @Translation("Site"),
 *   handlers = {
 *     "storage" = "Drupal\href_lang_exchange_href\SiteEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\href_lang_exchange_href\SiteEntityListBuilder",
 *     "views_data" = "Drupal\href_lang_exchange_href\Entity\SiteEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\href_lang_exchange_href\Form\SiteEntityForm",
 *       "add" = "Drupal\href_lang_exchange_href\Form\SiteEntityForm",
 *       "edit" = "Drupal\href_lang_exchange_href\Form\SiteEntityForm",
 *       "delete" = "Drupal\href_lang_exchange_href\Form\SiteEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\href_lang_exchange_href\SiteEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\href_lang_exchange_href\SiteEntityAccessControlHandler",
 *   },
 *   base_table = "site_entity",
 *   revision_table = "site_entity_revision",
 *   revision_data_table = "site_entity_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer site entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/site_entity/{site_entity}",
 *     "add-form" = "/admin/structure/site_entity/add",
 *     "edit-form" = "/admin/structure/site_entity/{site_entity}/edit",
 *     "delete-form" = "/admin/structure/site_entity/{site_entity}/delete",
 *     "version-history" = "/admin/structure/site_entity/{site_entity}/revisions",
 *     "revision" = "/admin/structure/site_entity/{site_entity}/revisions/{site_entity_revision}/view",
 *     "revision_revert" = "/admin/structure/site_entity/{site_entity}/revisions/{site_entity_revision}/revert",
 *     "revision_delete" = "/admin/structure/site_entity/{site_entity}/revisions/{site_entity_revision}/delete",
 *     "collection" = "/admin/structure/site_entity",
 *   },
 *   field_ui_base_route = "site_entity.settings"
 * )
 */
class SiteEntity extends EditorialContentEntityBase implements SiteEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the site_entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->get('url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return 'Site';
  }

  /**
   * {@inheritdoc}
   */
  public function getLowercaseLabel() {
    return 'site';
  }

  /**
   * {@inheritdoc}
   */
  public function getCollectionLabel() {
    return 'Site';
  }

  /**
   * {@inheritdoc}
   */
  public function getSingularLabel() {
    return 'Site';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluralLabel() {
    return 'Sites';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Administrative title'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Url'))
      ->setDescription(t('The Url for a subsite.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['href_lang_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The href lang code'))
      ->setDescription(t('Define the langcode en-GB.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
