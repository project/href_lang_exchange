<?php

namespace Drupal\href_lang_exchange_href\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Site entity entities.
 */
class SiteEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
