<?php

namespace Drupal\href_lang_exchange_href\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\href_lang_exchange\Connection\SiteInterface;

/**
 * Provides an interface for defining Site entity entities.
 *
 * @ingroup href_lang_exchange_href
 * (MasterSiteInterface)
 */
interface SiteEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, SiteInterface {


  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Site entity name.
   *
   * @return string
   *   Name of the Site entity.
   */
  public function getName();

  /**
   * Gets the Site entity name.
   *
   * @return string
   *   Name of the Site entity.
   */
  public function getTitle();

  /**
   * Sets the Site entity name.
   *
   * @param string $name
   *   The Site entity name.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   *   The called Site entity entity.
   */
  public function setName($name);

  /**
   * Gets the Site entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Site entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Site entity creation timestamp.
   *
   * @param int $timestamp
   *   The Site entity creation timestamp.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   *   The called Site entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Site entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Site entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   *   The called Site entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Site entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Site entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   *   The called Site entity entity.
   */
  public function setRevisionUserId($uid);

}
