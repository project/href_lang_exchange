<?php

namespace Drupal\href_lang_exchange_href\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\href_lang_exchange_href\Entity\SiteEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SiteEntityController.
 *
 *  Returns responses for Site entity routes.
 */
class SiteEntityController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new SiteEntityController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Site entity revision.
   *
   * @param int $site_entity_revision
   *   The Site entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($site_entity_revision) {
    $site_entity = $this->entityTypeManager()->getStorage('site_entity')
      ->loadRevision($site_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('site_entity');

    return $view_builder->view($site_entity);
  }

  /**
   * Page title callback for a Site entity revision.
   *
   * @param int $site_entity_revision
   *   The Site entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($site_entity_revision) {
    $site_entity = $this->entityTypeManager()->getStorage('site_entity')
      ->loadRevision($site_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $site_entity->label(),
      '%date' => $this->dateFormatter->format($site_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Site entity.
   *
   * @param \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface $site_entity
   *   A Site entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(SiteEntityInterface $site_entity) {
    $account = $this->currentUser();
    $site_entity_storage = $this->entityTypeManager()->getStorage('site_entity');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $site_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all site entity revisions") || $account->hasPermission('administer site entity entities')));
    $delete_permission = (($account->hasPermission("delete all site entity revisions") || $account->hasPermission('administer site entity entities')));

    $rows = [];

    $vids = $site_entity_storage->revisionIds($site_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface $revision */
      $revision = $site_entity_storage->loadRevision($vid);
      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
      if ($vid != $site_entity->getRevisionId()) {
        $link = $this->l($date, new Url('entity.site_entity.revision', [
          'site_entity' => $site_entity->id(),
          'site_entity_revision' => $vid,
        ]));
      }
      else {
        $link = $site_entity->link($date);
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      $row[] = $column;

      if ($latest_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];
        foreach ($row as &$current) {
          $current['class'] = ['revision-current'];
        }
        $latest_revision = FALSE;
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $this->t('Revert'),
            'url' => Url::fromRoute('entity.site_entity.revision_revert', [
              'site_entity' => $site_entity->id(),
              'site_entity_revision' => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.site_entity.revision_delete', [
              'site_entity' => $site_entity->id(),
              'site_entity_revision' => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }

      $rows[] = $row;
    }

    $build['site_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
