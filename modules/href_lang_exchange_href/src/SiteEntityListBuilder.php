<?php

namespace Drupal\href_lang_exchange_href;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\href_lang_exchange_href\Helper\CheckAvailabilityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Site entity entities.
 *
 * @ingroup href_lang_exchange_href
 */
class SiteEntityListBuilder extends EntityListBuilder {

  /**
   * The availability checker.
   *
   * @var \Drupal\href_lang_exchange_href\Helper\CheckAvailabilityInterface
   */
  protected $availabilityService;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('href_lang_exchange_href.helper.check_availability')
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\href_lang_exchange_href\Helper\CheckAvailabilityInterface $availability
   *   The availability service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, CheckAvailabilityInterface $availability) {
    parent::__construct($entity_type, $storage);
    $this->availabilityService = $availability;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Site entity ID');
    $header['name'] = $this->t('Name');
    $header['status'] = $this->t('Status');
    $header['sitemap'] = $this->t('Sitemap');
    $header['https'] = $this->t('HTTPS');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\href_lang_exchange_href\Entity\SiteEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.site_entity.edit_form',
      ['site_entity' => $entity->id()]
    );

    $url = $entity->getUrl();

    $siteIstAvailable = $this->availabilityService->isSiteAvailable($url);
    $row['status'] = $siteIstAvailable ? 'available' : 'not available';
    $validateSitemap = $this->availabilityService->hasValidateSitemap($url);
    $row['sitemap'] = $validateSitemap ? 'available' : 'not available';
    $validateSitemap = $this->availabilityService->isHttpsActive($url);
    $row['https'] = $validateSitemap ? 'available' : 'not available';

    return $row + parent::buildRow($entity);
  }

}
