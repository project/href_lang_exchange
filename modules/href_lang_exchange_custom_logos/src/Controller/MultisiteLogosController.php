<?php

namespace Drupal\href_lang_exchange_custom_logos\Controller;

use LasseRafn\InitialAvatarGenerator\InitialAvatar;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Psr7\Response;

/**
 * Defines a route controller for.
 */
class MultisiteLogosController extends ControllerBase {

  /**
   * Factory Method for the creation of a avatar genration class.
   *
   * @return \LasseRafn\InitialAvatarGenerator\InitialAvatar
   *   The avatar generation class
   */
  private function createInitialAvatar() {
    return new InitialAvatar();
  }

  /**
   * Create a image from the two given variables.
   *
   * @param string $name
   *   The short name for the flag.
   * @param string $color
   *   This is a hex code.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   The Response
   */
  public function handle($name, $color) {
    $avatar = $this->createInitialAvatar();

    if (is_string($name) && preg_match('/^[a-f0-9]{6}$/i', $color)) {
      return new Response(200, [
        'Cache-Control' => 'max-age=86400',
        'Content-Type' => 'image/png',
      ], $avatar->name($name)
        ->length(2)
        ->fontSize(0.5)
      // 48 * 2
        ->size(96)
        ->background('#' . $color)
        ->color('#fff')
        ->generate()
        ->stream('png', 100));
    }

    return new Response(404, []);
  }

  /**
   * Handle the dynamic generation of the css to include the logos.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   The css response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function handleCss() {
    $value = '';
    /** @var \Drupal\href_lang_exchange_href\SiteEntityStorageInterface $site_storage */
    $site_storage = \Drupal::entityTypeManager()
      ->getStorage('site_entity');

    /** @var \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface[] $site_entities */
    $site_entities = $site_storage->loadMultiple();

    foreach ($site_entities as $site_entity) {
      $short_title = '';
      $color_code = '';

      if ($site_entity->hasField('field_short_title')) {
        $short_title = $site_entity->get('field_short_title')->getString();
      }
      if ($site_entity->hasField('field_color_code')) {
        $color_code = $site_entity->get('field_color_code')->getString();
      }
      if (!empty($short_title)) {
        $value .= $this->createCssBackground($short_title, $color_code);
      }
    }

    if (!empty($value)) {
      return new Response(200, [
        'Content-Type' => 'text/css',
      ], $value);
    }

    return new Response(404, []);
  }

  /**
   * Create the css entry for a short name.
   *
   * @param string $short_name
   *   The short name for the flag.
   * @param null|string $color
   *   This is null or a hex code.
   *
   * @return string
   *   Return the css string.
   */
  private function createCssBackground($short_name, $color = NULL) {
    $return_value = '.flag-icon-' . mb_strtolower($short_name) . '.flag-icon-squared{background-image: url(/generate/icon/' . $short_name;
    if (!empty($color)) {
      $return_value .= '/' . $color;
    }
    $return_value .= ') !important;}';
    return $return_value;
  }

}
