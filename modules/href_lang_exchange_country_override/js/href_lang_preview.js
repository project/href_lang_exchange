/**
 * @file
 */

(function ($, Drupal, drupalSettings) {

    'use strict';

    Drupal.behaviors.href_lang_country_preview = {
        attach: function (context, settings) {

            var closure = function () {

                var country = $('.country_selector :selected').val();
                var language = $('.language_selector :selected').val();

                var output_string = '';
                var language_string = '';
                var country_string = '';

                if (country && language) {
                    language_string = $(".language_selector  option:selected").text();
                    var output_strings = [];

                    $(".language_config_site_fieldset .fieldset-wrapper .preview_container").text('');

                    $(".country_widget option:selected").each(function (element) {

                        if (!(isNaN($(this).text()))) {
                            return;
                        }
                        if ($(this).val()) {
                            output_strings.push(Drupal.t('@language-@country : @language_string language content, for @country_string user.', {
                                '@language': language.toLowerCase(),
                                '@language_string': language_string,
                                '@country': $(this).val().toUpperCase(),
                                '@country_string': $(this).text()
                            }));
                        }
                            output_strings.forEach(text => {
                                $(this).parent().parent().children('.preview-container').text(text)
                            });
                    }
                    );

                }

                if (!(country) && language) {
                    language_string = $(".language_selector  option:selected").text();
                    output_string = Drupal.t('@language :  @language_string language content, independent of region.', {
                        '@language': language.toLowerCase(),
                        '@language_string': language_string
                    });

                    $(this).parent().parent().children('.preview-container').text(output_string)
                }

                if (country && !(language)) {
                    $(".country_widget option:selected").each(function (element) {

                        if (!(isNaN($(this).text()))) {
                            return;
                        }
                        if ($(this).val()) {
                            console.log($(this).parent().parent().children('.preview-container').length === 0);
                            if ($(this).parent().parent().children('.preview-container').length === 0) {
                                $(this).parent().parent().append("<div class='preview-container'></div>");
                            }
                            var card = "<div class='card'>" + "<p>" + Drupal.t('@country : site language content, for @country_string user.', {
                                '@country': $(this).val().toUpperCase(),
                                '@country_string': $(this).text()
                            }) + "</p> </div>";
                            $(this).parent().parent().children('.preview-container').text('');
                            $(this).parent().parent().children('.preview-container').append(card);

                        }
                    });
                }
            };

            $('.country_widget select').on('change', closure);
            $('.language_config_site select').on('change', closure);

            closure();
        }
    };

})(jQuery, Drupal, drupalSettings);
