<?php

/**
 * @file
 * Enhances the href lang support in core.
 */

use Drupal\href_lang_exchange\Service\LazyStoreInterface;
use Drupal\Component\Utility\Tags;
use Drupal\href_lang_exchange\Service\NormalLazyStore;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\href_lang_exchange_href\Entity\SiteEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\href_lang_exchange\Entity\HrefLangItem;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Implements hook_form_alter().
 */
function href_lang_exchange_country_override_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['field_country']) && isset($form['href_lang_code'])) {

    $form['href_config_fieldset'] = [
      '#type' => 'fieldset',
      '#weight' => '13',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => ['class' => ['language_config_site_fieldset']],
    ];

    $form['href_config_fieldset']['href_config'] = [
      '#type' => 'container',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,

      '#attributes' => ['class' => ['language_config_site']],
      '#attached' => [
        'library' => ['href_lang_exchange_country_override/href_lang_preview'],
      ],
    ];

    $form['href_lang_code']['#attributes']['disabled'] = TRUE;

    RenderElement::setAttributes($form['field_country'], ['country_widget']);
    RenderElement::setAttributes($form['field_country']['widget'][0]['value'], ['country_selector']);

    $widgets_of_countries = Element::children($form['field_country']['widget']);
    foreach ($widgets_of_countries as $element) {
      if (
        is_int($element) &&
        $form['field_country']['widget']['#theme'] === 'field_multiple_value_form'
      ) {
        $form['field_country']['widget'][$element]['value']['#wrapper_attributes'] = ['class' => 'country_div_selector'];
        RenderElement::setAttributes($form['field_country']['widget'][$element]['value'], ['country_selector']);
      }
    }

    if (isset($form['field_language'])) {
      RenderElement::setAttributes($form['field_language']['widget'][0]['value'], ['language_selector']);
      $form['href_config_fieldset']['href_config']['field_language'] = $form['field_language'];
      unset($form['field_language']);
    }

    $form['href_config_fieldset']['href_config']['field_country'] = $form['field_country'];
    $form['href_config_fieldset']['href_config']['href_lang_code'] = $form['href_lang_code'];

    unset($form['field_country']);
    unset($form['href_lang_code']);

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    ];

    $form['revision'] = [
      '#type' => 'details',
      '#title' => t('Revision form settings'),
      '#group' => 'advanced',
      '#open' => TRUE,
    ];

    $form['workflow'] = [
      '#type' => 'details',
      '#title' => t('Publishing options'),
      '#group' => 'advanced',
    ];

    if (isset($form['new_revision'])) {
      $form['revision']['new_revision'] = $form['new_revision'];
      unset($form['new_revision']);
    }
    if (isset($form['revision_log_message'])) {
      $form['revision']['revision_log_message'] = $form['revision_log_message'];
      unset($form['revision_log_message']);
    }
    if (isset($form['new_revision'])) {
      $form['workflow']['status'] = $form['status'];
      unset($form['status']);
    }
  }
}

/**
 * Implements hook_entity_presave().
 */
function href_lang_exchange_country_override_site_entity_presave(EntityInterface $entity) {
  /** @var \Drupal\href_lang_exchange\Connection\ConnectionInterface $connection */
  $connection = \Drupal::service('href_lang_exchange.connection');

  /** @var \Drupal\href_lang_exchange\Connection\SiteManagementInterface $destination_service */
  $destination_service = \Drupal::service('href_lang_exchange.service.site_management_service');

  /** @var \Drupal\Component\Uuid\UuidInterface $generator */
  $generator = \Drupal::service('uuid');

  /** @var \Drupal\href_lang_exchange\HrefLangItemStorageInterface $href_lang_storage */
  try {
    $href_lang_storage = \Drupal::entityTypeManager()
      ->getStorage('href_lang_item');
    $master = $connection->getMaster();
  }
  catch (\Exception $e) {
    \Drupal::logger('href_lang_exchange_country_override')
      ->error('Automatic create frontpage HREF: ' . $e->getMessage());
  }
  $gid = $generator->generate();

  $element = $href_lang_storage->loadByInternalId('frontpage');
  $elements = $href_lang_storage->loadAllByInternalId('frontpage');

  $regions = [];
  $language = 'en';

  if ($connection->checkSelfMaster()) {

    if ($entity->hasField('field_country')) {
      $regions = $entity->get('field_country')->getValue();
    }

    if ($entity->hasField('field_language')) {
      $language = $entity->get('field_language')->value;
    }

    if (empty($region)) {
      $regions[] = ['value' => $language];
    }

    if ($element != NULL) {
      $gid = $element->getGid();
    }

    foreach ($regions as $region) {
      if (isset($region['value'])) {
        $href_lang_item = HrefLangItem::create([
          'gid' => $gid,
          'region' => $region,
          'language' => $language,
          'internal_id' => 'frontpage',
          'title' => $entity->getTitle(),
          'path' => $entity->getUrl(),
        ]);

        $elements[] = $href_lang_item;
      }
    }

    foreach ($elements as $element) {
      $connection->createHrefLangItem($master->getUrl(), $element->toJsonApi(), 'true');
    }
  }
}

/**
 * Implements hook_href_lang_creation_alter().
 */
function href_lang_exchange_country_override_href_lang_creation_alter(EntityInterface $entity, array &$entities, array $sites) {
  /** @var \Drupal\href_lang_exchange\Service\LazyStore $lazy_store */
  $lazy_store = \Drupal::service('href_lang_exchange.service.lazy_store');

  try {
    /** @var \Drupal\href_lang_exchange_href\SiteEntityStorageInterface $site_storage */
    $site_storage = \Drupal::entityTypeManager()
      ->getStorage('site_entity');
    /** @var \Drupal\href_lang_exchange\Connection\SendValidatorInterface $validation */
    $validation = \Drupal::service('href_lang_exchange.send_validation');
  }
  catch (\Exception $e) {
    \Drupal::logger('href_lang_exchange_country_override')
      ->error('Override href lang item: ' . $e->getMessage());
  }

  $region = [];
  $language = 'en';

  if ($validation->isItJsonApiRequest($entity)) {

    $match = $site_storage->findNearestUrlPrefixMatch($entity->getUrl());

    if ($match instanceof SiteEntityInterface) {
      if ($match->hasField('field_country')) {
        $values = $match->get('field_country')->getValue();
        foreach ($values as $value) {
          $region[] = strtoupper($value['value']);
        }
      }

      if ($match->hasField('field_language')) {
        $language = $match->get('field_language')->getValue();
      }

      if (empty($region) && !empty($language)) {
        $region = strtoupper($language);
      }

      if (!is_array($region) && !empty($region) && !empty($language)) {
        $entity->setLanguage($language);
        $entity->setRegion($region);
      }

      if (is_array($region) && !empty($region)) {

        if (!empty($entity->original)) {
          $url = $entity->original->getPath();

          /** @var \Drupal\href_lang_exchange\HrefLangItemStorageInterface $href_lang_storage */
          $href_lang_storage = \Drupal::entityTypeManager()
            ->getStorage('href_lang_item');
          $href_lang_items = $href_lang_storage->loadAllByPath($url);

          if (!empty($href_lang_items) && is_array($href_lang_items)) {
            if (count($region) !== count($href_lang_items)) {
              \Drupal::logger('href_lang_exchange_country_override')
                ->error('Region and href lang items are not in sync: Regions:' . count($region) . ', Href:' . count($href_lang_items));
            }
            unset($href_lang_items[$entity->id()]);

            $found_regions = [];

            foreach ($href_lang_items as $item) {
              $region_found = FALSE;
              foreach ($region as $region_element) {

                if ($item->getRegion() === $region_element) {
                  $region_found = TRUE;

                  $item->setLanguage($entity->getLanguage());
                  $item->setPath($entity->getPath());
                  $item->setGid($entity->getGid());
                  $item->setTitle($entity->getTitle());

                  $found_regions[$item->getRegion()] = $item;
                }
              }

              if ($region_found == FALSE) {
                // @todo what we do if a region is removed.
              }

            }

            foreach ($found_regions as $region => $item) {
              if ($item instanceof HrefLangItem) {
                $lazy_store->addItem(new NormalLazyStore($item, 'save', []));
              }
              else {
                $entity_duplicate = $entity->createDuplicate();
                if (!empty($language)) {
                  $entity_duplicate->setLanguage($language);
                }
                $entity_duplicate->setRegion($region_element);
                $lazy_store->addItem(new NormalLazyStore($entity_duplicate, 'save', []));
              }
            }
          }
          else {
            __handle_multisite_creation($entity, $region, $lazy_store, $entities);
          }

        }
        else {
          __handle_multisite_creation($entity, $region, $lazy_store, $entities);
        }
      }

    }
  }

}

/**
 * Create multiple href_lang_items (for each region one.)
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The current entity.
 * @param array $region
 *   All regions of this site.
 * @param \Drupal\href_lang_exchange\Service\LazyStoreInterface $lazy_store
 *   A store to execute things lazy.
 * @param mixed $entities
 *   All other entities that need to be created in this language.
 */
function __handle_multisite_creation(EntityInterface $entity, array $region, LazyStoreInterface $lazy_store, &$entities) {
  // To remove infinity loop.
  \Drupal::request()->attributes->add(['_create_multiple' => TRUE]);

  $first = array_shift($region);

  if (!empty($language)) {
    $entity->setLanguage($language);
  }
  $entity->setRegion($first);

  foreach ($region as $region_element) {
    // Find a solution for the creation.
    $entity_duplicate = $entity->createDuplicate();
    if (!empty($language)) {
      $entity_duplicate->setLanguage($language);
    }
    $entity_duplicate->setRegion($region_element);
    $entities[] = $entity_duplicate;

    $lazy_store->addItem(new NormalLazyStore($entity_duplicate, 'save', []));
  }
}

/**
 * Implements hook_href_lang_autocomplete_alter().
 */
function href_lang_exchange_country_override_href_lang_autocomplete_alter(QueryInterface $query, Request $request) {
  $filter = [];
  try {
    /** @var \Drupal\href_lang_exchange_href\SiteEntityStorageInterface $site_storage */
    $site_storage = \Drupal::entityTypeManager()
      ->getStorage('site_entity');

    if ($site_wrapper = $request->query->get('site')) {
      $typed_site_string = Tags::explode($site_wrapper);
      $typed_site_string = mb_strtolower(array_pop($typed_site_string));
      $site_entity = $site_storage->findNearestUrlPrefixMatch('//' . $typed_site_string);
    }

    if (empty($site_entity) && isset($_SERVER['HTTP_ORIGIN'])) {
      $site_entity = $site_storage->findNearestUrlPrefixMatch($_SERVER['HTTP_ORIGIN']);
    }
    if (!empty($site_entity)) {

      $query_groups = \Drupal::entityQueryAggregate('href_lang_item');

      $condition_or = $query_groups->orConditionGroup();

      if ($site_entity->hasField('field_country')) {
        $regions = $site_entity->get('field_country')->getValue();
      }

      if ($site_entity->hasField('field_language')) {
        $language = $site_entity->get('field_language')->value;
      }

      if (empty($language)) {
        if ($language_wrapper = $request->query->get('sub_lang')) {
          $typed_language_string = Tags::explode($language_wrapper);
          $typed_language_string = mb_strtolower(array_pop($typed_language_string));
          $language = $typed_language_string;
        }
      }

      if (!empty($language)) {
        foreach ($regions as $region) {
          $condition = $query_groups->andConditionGroup();
          $condition->condition('language', strtolower($language), '=');
          $condition->condition('region', strtoupper($region['value']), '=');
          $condition_or->condition($condition);
        }
      }

      $query_groups->groupBy('gid')
        ->aggregate('id', 'count');

      $query_groups->condition($condition_or);
      $result_set = $query_groups->execute();

      foreach ($result_set as $item) {
        $filter[] = $item['gid'];
      }

      if (!empty($filter)) {
        if ($ignore_wrapper = $request->query->get('ignore')) {
          $typed_ignore_string = Tags::explode($ignore_wrapper);
          $typed_ignore_string = mb_strtolower(array_pop($typed_ignore_string));
          $filter = array_filter($filter, function ($el) use ($typed_ignore_string) {
            return $el === $typed_ignore_string ? FALSE : TRUE;
          });
        }
      }

      if (!empty($filter)) {
        $query->condition('gid', $filter, 'NOT IN');
      }
    }

  }
  catch (\Exception $e) {
    \Drupal::logger('href_lang_exchange_country_override')
      ->alert('hook_href_lang_autocomplete_alter() :' . $e->getMessage());
  }

}
