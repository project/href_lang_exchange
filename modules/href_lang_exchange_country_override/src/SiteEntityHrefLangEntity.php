<?php

namespace Drupal\href_lang_exchange_country_override;

use Drupal\href_lang_exchange\HrefLangItemInterface;
use Drupal\href_lang_exchange_href\Entity\SiteEntityInterface;

/**
 * Service relation between site entity and href langage .
 *
 * @package Drupal\href_lang_exchange_country_override
 */
class SiteEntityHrefLangEntity {

  /**
   * The site entity.
   *
   * @var \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   */
  protected $siteEntity;

  /**
   * The href lang item.
   *
   * @var \Drupal\href_lang_exchange\HrefLangItemInterface
   */
  protected $hrefLangItem;

  /**
   * SiteEntityHrefLangEntity constructor.
   *
   * @param \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface $site_entity
   *   The site entity.
   * @param \Drupal\href_lang_exchange\HrefLangItemInterface $hrefLangItem
   *   The href lang item.
   */
  public function __construct(SiteEntityInterface $site_entity, HrefLangItemInterface $hrefLangItem) {
    $this->siteEntity = $site_entity;
    $this->hrefLangItem = $hrefLangItem;
  }

  /**
   * Return the href lang item.
   *
   * @return \Drupal\href_lang_exchange\HrefLangItemInterface
   *   Return the href lang item.
   */
  public function getHref() {
    return $this->hrefLangItem;
  }

  /**
   * Return the site entity.
   *
   * @return \Drupal\href_lang_exchange_href\Entity\SiteEntityInterface
   *   Return the site entity.
   */
  public function getSite() {
    return $this->siteEntity;

  }

}
