<?php

namespace Drupal\href_lang_exchange_country_override;

use Drupal\href_lang_exchange_href\Entity\SiteEntityInterface;

/**
 * Service Region Language Filter.
 *
 * @package Drupal\href_lang_exchange_country_override
 */
class RegionLanguageFilter implements RegionLanguageFilterInterface {

  /**
   * {@inheritdoc}
   */
  public function filterByDuplicatePath($matches) {
    $filter = [];
    $remove_list = [];
    $elements = $matches;

    foreach ($elements as $key => $match) {
      if (isset($filter[$match['url']])) {
        $remove_list[] = $key;
      }
      else {
        $filter[$match['url']] = $key;
      }
    }

    foreach ($remove_list as $element) {
      unset($matches[$element]);
    }

    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public function filterByDuplicateGid($matches) {
    $filter = [];
    $remove_list = [];
    $elements = $matches;
    foreach ($elements as $key => $match) {
      if (isset($filter[$match['gid']])) {
        $remove_list[] = $key;
      }
      else {
        $filter[$match['gid']] = $key;
      }
    }

    foreach ($remove_list as $element) {
      unset($matches[$element]);
    }

    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public function filterBySiteEntityRegion(SiteEntityInterface $site_entity, $match) {
    $countries = [];
    if ($site_entity->hasField('field_country')) {
      $countries = $site_entity->get('field_country')->getValue();
    }

    if ($site_entity->hasField('field_short_title')) {
      $short_title = $site_entity->get('field_short_title')->getString();
    }

    // Filter subelements by region gid == main gid.
    foreach ($countries as $country) {
      if (isset($match['elements'])) {
        $match['elements'] = array_filter($match['elements'], function ($el) use ($country, $short_title) {
          if (
            isset($el['region']) &&
            strtoupper($el['region']) === strtoupper($country['value'])
          ) {
            return FALSE;
          }
          $el['short_title'] = $short_title;
          return TRUE;
        });
      }
    }

    $match['elements'] = array_values($match['elements']);

    $match['short_title'] = $short_title;
  }

}
