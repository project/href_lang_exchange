<?php

namespace Drupal\href_lang_exchange_country_override;

use Drupal\href_lang_exchange\HrefLangItemInterface;
use Drupal\href_lang_exchange\Service\LazyStoreInterface;
use Drupal\href_lang_exchange\Service\NormalLazyStore;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provide Site Entity multi region storage.
 *
 * @package Drupal\href_lang_exchange_country_override
 *
 * @todo Check if this class is used.
 */
class SiteEntityMultiRegionExtensionStorage implements SiteEntityMultiRegionExtensionStorageInterface {

  /**
   * The lazy store.
   *
   * @var \Drupal\href_lang_exchange\Service\LazyStoreInterface
   */
  protected $lazyStore;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * SiteEntityMultiRegionExtensionStorage constructor.
   *
   * @param \Drupal\href_lang_exchange\Service\LazyStoreInterface $lazy_store
   *   The lazy store.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request.
   */
  public function __construct(LazyStoreInterface $lazy_store, RequestStack $requestStack) {
    $this->lazyStore = $lazy_store;
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * Create for each given region a hreflangitem like the current entity.
   *
   * @param \Drupal\href_lang_exchange\HrefLangItemInterface $entity
   *   The current entity.
   * @param array $region
   *   The region array.
   * @param array $entities
   *   All entities.
   */
  public function createMultipleHrefLangItemsBySiteRegion(HrefLangItemInterface $entity, array $region, array &$entities) {
    $this->request->attributes->add(['_create_multiple' => TRUE]);

    $first = array_shift($region);

    if (!empty($language)) {
      $entity->setLanguage($language);
    }
    $entity->setRegion($first);

    foreach ($region as $region_element) {
      // Find a solution for the creation.
      $entity_duplicate = $entity->createDuplicate();
      if (!empty($language)) {
        $entity_duplicate->setLanguage($language);
      }
      $entity_duplicate->setRegion($region_element);
      $entities[] = $entity_duplicate;

      $this->lazyStore->addItem(new NormalLazyStore($entity_duplicate, 'save', []));
    }
  }

}
